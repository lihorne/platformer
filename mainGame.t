<<<<<<< HEAD
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROJECT: REVOLUTION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

var iWindow : int := Window.Open ("graphics:max;max,offscreenonly,nobuttonbar,noecho,nocursor,title:PROJECT: REVOLUTION")

%%%%%%%%%%%%%%%%%%%%FONT VARIABLES%%%%%%%%%%%%%%%%%%%%%
var font1 : int := Font.New ("calibri:40")
var font2 : int := Font.New ("calibri:30")
var font3 : int := Font.New ("calibri:25")
var font4 : int := Font.New ("calibri:20")
var font5 : int := Font.New ("calibri:15")
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%Menu Variables%%%%%%%%%%%%
var mouseX, mouseY, mouseButton : int
var colour1, colour2, colourOutline, colourPlatforms, colourBackground, colourBorder : int
colour1 := 54
colour2 := 7
colourOutline := 19
colourPlatforms := 53
colourBackground := 7
colourBorder := 54
var choice : string
var goToGame : boolean := false
var goToHelp : boolean := false
var dropDownCharacter := false
var dropDownLevel := false
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%Level Variables%%%%%%%%%%%
var goToMenu, Restart, canHasLevelUp := false
var levelNo : int
var onWinPlatformCount : real := 0
var levelUp : boolean := false
var gameWon := false
var gameWon2 := false
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%Structural Variables%%%%%%%
type barrier :
    record
  left : int
	right : int
	top : int
	bottom : int
    end record
var platforms : flexible array 1 .. 0 of barrier
var endPoint : barrier
var walls : flexible array 1 .. 0 of barrier
type movingBarrier :
    record
	left, right, bottom, top : int
	leftBound, rightBound, topBound, bottomBound : int
	xVelocity, yVelocity : int
    end record
var movingPlatform : flexible array 1 .. 0 of movingBarrier
var colorbase : int
var onPlatform, onWall, onMovingPlatform : boolean := false
var scroll : boolean := true
var unlocked : flexible array 1 .. 0 of boolean
var unlockedW : flexible array 1 .. 0 of boolean
var unlockedM : flexible array 1 .. 0 of boolean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%Input Variables%%%%%%%%%%%%%%%
var chars : array char of boolean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%Character Variables%%%%%%%%%%%%
var playerX : int := maxx div 2
var playerWidth : int := 10
var playerX2 := playerX + playerWidth
var playerY : int := 1
var playerYStoreValue : int := playerY
var playerHeight : int := 20
var playerY2 := playerY + playerHeight
var playerYVelocity, squatCount : real := 0
var first, second, third, fourth : int
first := 0
second := 90
third := 180
fourth := 270
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%Constants%%%%%
const gravity := 1
const rollValue := 30
const ScrollValue := 10
const ballRadius := 10
const YouWinLevel := 5
%%%%%%%%%%%%%%%%%%'

proc GetLevels
    include "Levels\platforms.t"
    include "Levels\walls.t"
    include "Levels\movingPlatform.t"
end GetLevels

%%%%%%%%%%%%%%%%%%Player%%%%%%%%%%%%%%%
proc DrawBall
    if playerHeight = 20 then
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius, ballRadius + 1, ballRadius + 1, colourOutline)
    elsif playerHeight = 10 and (onPlatform or onWall or onMovingPlatform) then %squished ball
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 3, ballRadius - 2, colourOutline)
    elsif playerHeight = 10 then % squishing in the air has no effect
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius, ballRadius + 1, ballRadius + 1, colourOutline)
    end if
end DrawBall

proc DrawMan
    playerX2 := playerX + playerWidth
    playerY2 := playerY + playerHeight
    Draw.FillBox (playerX + 1, playerY + 1, playerX2 - 1, playerY2 - 1, 0)
    Draw.Box (playerX - 1, playerY - 1, playerX2 + 1, playerY2 + 1, colourOutline)
    if playerHeight = 20 then
	drawfillbox (playerX - 3, playerY2 - 1, playerX2 + 3, playerY2 + 4, colour1)
	drawfillbox (playerX + 2, playerY2 + 6, playerX2 - 2, playerY2 + 8, colour1)
	drawbox (playerX - 4, playerY2 - 2, playerX2 + 4, playerY2 + 5, colourOutline)
	drawbox (playerX + 4, playerY2 + 7, playerX2 - 4, playerY2 + 8, colourOutline)
	drawfillbox (playerX, playerY2 + 4, playerX2, playerY2 + 6, colour2)
	drawfillbox (playerX + 1, playerY2 - 5, playerX + 3, playerY2 - 3, 7)
	drawfillbox (playerX2 - 1, playerY2 - 5, playerX2 - 3, playerY2 - 3, 7)
	Draw.ThickLine (playerX + 1, playerY2 - 8, playerX2 - 1, playerY2 - 8, 2, 7)
    elsif playerHeight = 10 then %crouching
	drawfillbox (playerX - 3, playerY - 3, playerX2 + 3, playerY2 + 3, colour1)
	drawfillbox (playerX, playerY + 1, playerX2, playerY2, colour2)
	drawfillbox (playerX + 3, playerY + 4, playerX2 - 3, playerY2 - 3, colour1)
    end if
end DrawMan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

include "Include/colours.t" %color scheme selection program
include "Include/startMenu.t"

%%%%%%%%%%%%%%%%%%%%%COLLISION%%%%%%%%%%%%%%%%%%
%Every procedure checks if the player is inside or beside the structure and then moves the player so that he is on top of the structure
%(in the case of a platform) or beside it (in the case of a wall). There is also a flag that changes the structure's color.

proc checkPlatformCollision
    for i : 1 .. upper (platforms)
	if playerX2 >= platforms (i).left and playerX <= platforms (i).right and playerY2 <= platforms (i).top + playerHeight + 1
		and playerY >= platforms (i).bottom and playerYVelocity <= 0 then
	    playerY := platforms (i).top + 1
	    playerYVelocity := 0 %stopps the player from falling through the structure
	    unlocked (i) := true
	else
	    unlocked (i) := false
	end if
    end for
    %this checks for what to light up
    for i : 1 .. upper (unlocked)
	if unlocked (i) then
	    onPlatform := true
	else
	    onPlatform := false
	end if
	exit when onPlatform
    end for
end checkPlatformCollision

proc checkOnEndPoint
    if playerX2 >= endPoint.left - 0 and playerX <= endPoint.right + 0 and playerY2 <= endPoint.top + playerHeight + 1 and playerY >= endPoint.bottom
	    and playerYVelocity <= 0 then
	playerY := endPoint.top + 1
	playerYVelocity := 0
	canHasLevelUp := true % this is a flag that advances the game to the next level
    else
	canHasLevelUp := false
    end if
end checkOnEndPoint

proc checkMovingPCollision
    for i : 1 .. upper (movingPlatform)
	if playerX2 >= movingPlatform (i).left and playerX <= movingPlatform (i).right and playerY2 <= movingPlatform (i).top + 21 + abs (movingPlatform (i).yVelocity)
		and playerY >= movingPlatform (i).bottom - abs (movingPlatform (i).yVelocity) and playerYVelocity <= 0 then
	    playerY := movingPlatform (i).top + 1
	    playerYVelocity := 0
	    unlockedM (i) := true
	else
	    unlockedM (i) := false
	end if
    end for
    for i : 1 .. upper (unlockedM)
	if unlockedM (i) then
	    onMovingPlatform := true
	else
	    onMovingPlatform := false
	end if
	exit when onMovingPlatform
    end for
end checkMovingPCollision

proc checkWallCollision
    for i : 1 .. upper (walls)

	if playerY2 <= walls (i).top and playerY >= walls (i).bottom then
	    if playerX2 <= walls (i).right and playerX2 >= walls (i).left then
		scroll := false % cannot move into wall
		%%%%% moves the player out of a wall
		for j : 1 .. upper (platforms)
		    platforms (j).left += ScrollValue
		    platforms (j).right += ScrollValue
		end for
		for j : 1 .. upper (movingPlatform)
		    movingPlatform (j).left += ScrollValue
		    movingPlatform (j).right += ScrollValue
		    movingPlatform (j).leftBound += ScrollValue
		    movingPlatform (j).rightBound += ScrollValue
		end for
		for j : 1 .. upper (walls)
		    walls (j).left += ScrollValue
		    walls (j).right += ScrollValue
		end for
		endPoint.left += ScrollValue
		endPoint.right += ScrollValue
		if choice = "ball" then
		    first += rollValue
		    second += rollValue
		    third += rollValue
		    fourth += rollValue
		end if
		%%%%%%%
	    elsif playerX <= walls (i).right + 2 and playerX >= walls (i).left then
		scroll := false
		for j : 1 .. upper (platforms)
		    platforms (j).left -= ScrollValue
		    platforms (j).right -= ScrollValue
		end for
		for j : 1 .. upper (movingPlatform)
		    movingPlatform (j).left -= ScrollValue
		    movingPlatform (j).right -= ScrollValue
		    movingPlatform (j).leftBound -= ScrollValue
		    movingPlatform (j).rightBound -= ScrollValue
		end for
		for j : 1 .. upper (walls)
		    walls (j).left -= ScrollValue
		    walls (j).right -= ScrollValue
		end for
		endPoint.left -= ScrollValue
		endPoint.right -= ScrollValue
		if choice = "ball" then
		    first -= rollValue
		    second -= rollValue
		    third -= rollValue
		    fourth -= rollValue
		end if
	    else
		scroll := true
	    end if
	end if
	if playerX2 >= walls (i).left and playerX <= walls (i).right and playerY2 <= walls (i).top + playerHeight + 1
		and playerY >= walls (i).top - 10 and playerYVelocity <= 0 then
	    playerY := walls (i).top + 1
	    playerYVelocity := 0
	    unlockedW (i) := true
	elsif playerX2 >= walls (i).left and playerX <= walls (i).right and playerY2 <= walls (i).top
		and playerY >= walls (i).bottom - playerHeight - 5 and playerYVelocity > 0 then
	    playerY := walls (i).bottom - playerHeight - 1
	    playerYVelocity := 0
	else
	    unlockedW (i) := false
	end if
    end for
    for i : 1 .. upper (unlockedW)
	if unlockedW (i) then
	    onWall := true
	else
	    onWall := false
	end if
	exit when onWall
    end for
end checkWallCollision
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%PLATFORM MOVEMENT%%%%%%%%%%%%%%%%%
proc platformMovement
    for i : 1 .. upper (movingPlatform)
	%%%%%%%%%platform stays within its given territory
	if movingPlatform (i).left <= movingPlatform (i).leftBound and movingPlatform (i).xVelocity < 0
		or movingPlatform (i).right >= movingPlatform (i).rightBound and movingPlatform (i).xVelocity > 0 then
	    movingPlatform (i).xVelocity := -movingPlatform (i).xVelocity
	end if
	if movingPlatform (i).top >= movingPlatform (i).topBound and movingPlatform (i).yVelocity > 0
		or movingPlatform (i).bottom <= movingPlatform (i).bottomBound and movingPlatform (i).yVelocity < 0
		then
	    movingPlatform (i).yVelocity := -movingPlatform (i).yVelocity
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%vertical movement
	movingPlatform (i).top += movingPlatform (i).yVelocity
	movingPlatform (i).bottom += movingPlatform (i).yVelocity
	%%%
	if unlockedM (i) = false then %if the player is not on the platform, horizontal movement
	    movingPlatform (i).left += movingPlatform (i).xVelocity
	    movingPlatform (i).right += movingPlatform (i).xVelocity
	else % if he is, everything else moves
	    for j : 1 .. upper (platforms)
		platforms (j).left -= movingPlatform (i).xVelocity
		platforms (j).right -= movingPlatform (i).xVelocity
	    end for
	    for j : 1 .. upper (movingPlatform)
		if j = i then
		    movingPlatform (j).leftBound -= movingPlatform (i).xVelocity
		    movingPlatform (j).rightBound -= movingPlatform (i).xVelocity
		else
		    movingPlatform (j).left -= movingPlatform (i).xVelocity
		    movingPlatform (j).right -= movingPlatform (i).xVelocity
		    movingPlatform (j).leftBound -= movingPlatform (i).xVelocity
		    movingPlatform (j).rightBound -= movingPlatform (i).xVelocity
		end if
	    end for
	    for j : 1 .. upper (walls)
		walls (j).left -= movingPlatform (i).xVelocity
		walls (j).right -= movingPlatform (i).xVelocity
	    end for
	    endPoint.left -= movingPlatform (i).xVelocity
	    endPoint.right -= movingPlatform (i).xVelocity
	end if
    end for
end platformMovement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%USER INPUT%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc Buttons
    Input.KeyDown (chars)
    if chars (KEY_DOWN_ARROW) or chars ('s') then %crouching, falling through ground
	playerHeight := 10
	if onPlatform then
	    squatCount += 1
	end if
	if onMovingPlatform then
	    squatCount += 1
	end if
    end if
    if (chars (KEY_UP_ARROW) or chars ('w')) and (onPlatform or onWall or onMovingPlatform) then % must be on a structure to jump
	playerYVelocity += 20
    end if
    if chars (KEY_LEFT_ARROW) or chars ('a') and scroll = true then %everything moves rather than the player
	for i : 1 .. upper (platforms)
	    platforms (i).left += ScrollValue
	    platforms (i).right += ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left += ScrollValue
	    movingPlatform (i).right += ScrollValue
	    movingPlatform (i).leftBound += ScrollValue
	    movingPlatform (i).rightBound += ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left += ScrollValue
	    walls (i).right += ScrollValue
	end for
	endPoint.left += ScrollValue
	endPoint.right += ScrollValue
	first += rollValue
	second += rollValue
	third += rollValue
	fourth += rollValue
    end if
    if chars (KEY_RIGHT_ARROW) or chars ('d') and scroll = true then
	for i : 1 .. upper (platforms)
	    platforms (i).left -= ScrollValue
	    platforms (i).right -= ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left -= ScrollValue
	    movingPlatform (i).right -= ScrollValue
	    movingPlatform (i).leftBound -= ScrollValue
	    movingPlatform (i).rightBound -= ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left -= ScrollValue
	    walls (i).right -= ScrollValue
	end for
	endPoint.left -= ScrollValue
	endPoint.right -= ScrollValue
	first -= rollValue
	second -= rollValue
	third -= rollValue
	fourth -= rollValue
    end if
    if chars (KEY_ESC) then
	goToMenu := true     %exit game
    elsif chars ('r') then
	Restart := true
    end if
end Buttons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%DRAW STRUCTURES%%%%%%%%%%%%%%%%%%%%
proc drawPlatform
    for i : 1 .. upper (platforms)
	if unlocked (i) = false then %if the player is not on the platform
	    colorbase := 17
	else %if the player is on the platform
	    colorbase := colourPlatforms
	end if
	%draw the platform
	Draw.FillBox (platforms (i).left + 1, platforms (i).bottom + 1, platforms (i).right - 1, platforms (i).top - 1, colorbase)
	Draw.Box (platforms (i).left, platforms (i).bottom, platforms (i).right, platforms (i).top, colourBorder)
    end for
end drawPlatform

proc drawMovingPlatform
    for i : 1 .. upper (movingPlatform)
	if unlockedM (i) = false then
	    colorbase := 17
	else
	    colorbase := colourPlatforms
	end if
	Draw.FillBox (movingPlatform (i).left + 1, movingPlatform (i).bottom + 1, movingPlatform (i).right - 1, movingPlatform (i).top - 1, colorbase)
	Draw.Box (movingPlatform (i).left, movingPlatform (i).bottom, movingPlatform (i).right, movingPlatform (i).top, colourBorder)
    end for
end drawMovingPlatform

proc drawWall
    for i : 1 .. upper (walls)
	if unlockedW (i) = false then
	    colorbase := 17
	else
	    colorbase := colourPlatforms
	end if
	Draw.FillBox (walls (i).left + 1, walls (i).bottom + 1, walls (i).right - 1, walls (i).top - 1, colorbase)
	Draw.Box (walls (i).left, walls (i).bottom, walls (i).right, walls (i).top, colourBorder)
    end for
end drawWall

proc drawEndPoint
    if canHasLevelUp then
	onWinPlatformCount += 1
	colorbase := brightgreen
    else
	onWinPlatformCount := 0
	colorbase := 17
    end if
    Draw.FillBox (endPoint.left + 1, endPoint.bottom + 1, endPoint.right - 1, endPoint.top - 1, colorbase)
    Draw.Box (endPoint.left, endPoint.bottom, endPoint.right, endPoint.top, colourBorder)
end drawEndPoint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%MOVESCREEN%%%%%%%%%%%%
%for moving above maxy
proc MoveScreenUp % everything moves down a screen's worth, showing one screen above
    for i : 1 .. upper (platforms)
	platforms (i).top -= maxy
	platforms (i).bottom -= maxy
    end for
    for i : 1 .. upper (walls)
	walls (i).top -= maxy
	walls (i).bottom -= maxy
    end for
    for i : 1 .. upper (movingPlatform)
	movingPlatform (i).top -= maxy
	movingPlatform (i).bottom -= maxy
	movingPlatform (i).topBound -= maxy
	movingPlatform (i).bottomBound -= maxy
    end for
    endPoint.top -= maxy
    endPoint.bottom -= maxy
    playerY := 1
end MoveScreenUp
%for moving below 0
proc MoveScreenDown
    for i : 1 .. upper (platforms)
	platforms (i).top += maxy
	platforms (i).bottom += maxy
    end for
    for i : 1 .. upper (walls)
	walls (i).top += maxy
	walls (i).bottom += maxy
    end for
    for i : 1 .. upper (movingPlatform)
	movingPlatform (i).top += maxy
	movingPlatform (i).bottom += maxy
	movingPlatform (i).topBound += maxy
	movingPlatform (i).bottomBound += maxy
    end for
    endPoint.top += maxy
    endPoint.bottom += maxy
    playerY := maxy
end MoveScreenDown

%%%%%%%%%%%%%%%%%Character Updating%%%%%%%%%%%%%%%
proc Jumping
    % gravity pulls if in the air
    if onPlatform = false then
	playerYVelocity -= gravity
    end if
    %if player falls below screen, show area below it
    if playerY < 1 then
	MoveScreenDown
    end if
    %like wise for above
    if playerY > maxy then
	MoveScreenUp
    end if
    %maximum fall rate
    if playerYVelocity < -12 then
	playerYVelocity := -12
	%drop below a platform
    elsif squatCount = 20 then
	if onMovingPlatform = true or onPlatform = true then
	    playerY -= 25
	end if
	squatCount := 0
	%squatcount resets player isn't squatting
    elsif playerHeight = 20 then
	squatCount := 0
    end if
    playerY += round (playerYVelocity)

    if choice = "man" then
	playerWidth := 10
    else
	playerWidth := 20
    end if
    playerX2 := playerX + playerWidth
    playerY2 := playerY + playerHeight
end Jumping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc ResetLevel %reset everything within the level
    playerY := 1
    if choice = "man" then
	playerWidth := 10
    else
	playerWidth := 20
    end if
    playerX := maxx div 2 - playerWidth div 2
    playerHeight := 20
    playerYVelocity := 0
    squatCount := 0
    onWinPlatformCount := 0
    onPlatform := false
    onWall := false
    onMovingPlatform := false
    canHasLevelUp := false
    scroll := true
    Restart := false
    first := 0
    second := 90
    third := 180
    fourth := 270
end ResetLevel


%%%%%%%%%%%%%%%%%%%MAIN PROGRAM%%%%%%%%%%
choice := "ball"
loop
    Music.PlayFileLoop ("Music/Blizzard.mp3")
    loop
	startMenu
	exit when startNewGame or resumeGame or exitGame or levelSelected
    end loop
    if levelSelected then
	levelNo := selectedLevel
	GetLevels
	ResetLevel
	levelSelected := false
    end if
    if exitGame then
	Music.PlayFileStop
	exit
    end if
    if startNewGame then
	levelNo := 1
	GetLevels
	ResetLevel
    end if
    Music.PlayFileStop
    Music.PlayFileLoop ("Music/Session_1.mp3")
    loop
	if levelUp = true then
	    GetLevels
	    playerY := walls (1).top + 1
	    levelUp := false
	end if
	colorback (colourBackground)
	loop
	    cls
	    playerHeight := 20
	    platformMovement
	    Buttons
	    checkPlatformCollision
	    checkMovingPCollision
	    checkWallCollision
	    checkOnEndPoint
	    Jumping
	    checkPlatformCollision
	    checkMovingPCollision
	    checkWallCollision
	    checkOnEndPoint
	    drawPlatform
	    drawMovingPlatform
	    drawWall
	    drawEndPoint

	    if choice = "ball" then
		DrawBall
	    elsif choice = "man" then
		DrawMan
	    end if

	    Input.Flush
	    View.Update
	    Time.DelaySinceLast (15)
	    if onWinPlatformCount = 20 then
		if levelNo = YouWinLevel then
		    gameWon := true
		else
		    levelNo += 1
		    levelUp := true
		end if
		exit
	    end if
	    if goToMenu or Restart then
		exit
	    end if
	end loop
	cls
	if gameWon then
	    startNewGame := false
	    resumeGame := false
	    goToHelp := false
	    gameFirstStartup := false
	    gameWon := false
	    gameWon2 := true
	    exit
	end if
	if Restart then
	    ResetLevel
	    GetLevels
	elsif goToMenu then
	    %reset level data
	    startNewGame := false
	    resumeGame := false
	    goToHelp := false
	    playerYStoreValue := playerY
	    exit
	end if
    end loop
    Music.PlayFileStop
    goToMenu := false
end loop
Music.PlayFileStop
Window.Close (iWindow)
=======
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% PROJECT: REVOLUTION %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%

var iWindow : int := Window.Open ("graphics:max;max,offscreenonly,nobuttonbar,noecho,nocursor,title:PROJECT: REVOLUTION")

%%%%%%%%%%%%%%%%%%%%FONT VARIABLES%%%%%%%%%%%%%%%%%%%%%
var font1 : int := Font.New ("calibri:40")
var font2 : int := Font.New ("calibri:30")
var font3 : int := Font.New ("calibri:25")
var font4 : int := Font.New ("calibri:20")
var font5 : int := Font.New ("calibri:15")
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%Menu Variables%%%%%%%%%%%%
var mouseX, mouseY, mouseButton : int
var colour1, colour2, colourOutline, colourPlatforms, colourBackground, colourBorder : int
colour1 := 54
colour2 := 7
colourOutline := 19
colourPlatforms := 53
colourBackground := 7
colourBorder := 54
var choice : string
var goToGame : boolean := false
var goToHelp : boolean := false
var dropDownCharacter := false
var dropDownLevel := false
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%Level Variables%%%%%%%%%%%
var goToMenu, Restart, canHasLevelUp := false
var levelNo : int
var onWinPlatformCount : real := 0
var levelUp : boolean := false
var gameWon := false
var gameWon2 := false
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%Structural Variables%%%%%%%
type barrier :
    record
	left : int
	right : int
	top : int
	bottom : int
    end record
var platforms : flexible array 1 .. 0 of barrier
var endPoint : barrier
var walls : flexible array 1 .. 0 of barrier
type movingBarrier :
    record
	left, right, bottom, top : int
	leftBound, rightBound, topBound, bottomBound : int
	xVelocity, yVelocity : int
    end record
var movingPlatform : flexible array 1 .. 0 of movingBarrier
var colorbase : int
var onPlatform, onWall, onMovingPlatform : boolean := false
var scroll : boolean := true
var unlocked : flexible array 1 .. 0 of boolean
var unlockedW : flexible array 1 .. 0 of boolean
var unlockedM : flexible array 1 .. 0 of boolean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%Input Variables%%%%%%%%%%%%%%%
var chars : array char of boolean
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%Character Variables%%%%%%%%%%%%
var playerX : int := maxx div 2
var playerWidth : int := 10
var playerX2 := playerX + playerWidth
var playerY : int := 1
var playerYStoreValue : int := playerY
var playerHeight : int := 20
var playerY2 := playerY + playerHeight
var playerYVelocity, squatCount : real := 0
var first, second, third, fourth : int
first := 0
second := 90
third := 180
fourth := 270
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%Constants%%%%%
const gravity := 1
const rollValue := 30
const ScrollValue := 10
const ballRadius := 10
const YouWinLevel := 5
%%%%%%%%%%%%%%%%%%'

proc GetLevels
    include "Levels\platforms.t"
    include "Levels\walls.t"
    include "Levels\movingPlatform.t"
end GetLevels

%%%%%%%%%%%%%%%%%%Player%%%%%%%%%%%%%%%
proc DrawBall
    if playerHeight = 20 then
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius, ballRadius + 1, ballRadius + 1, colourOutline)
    elsif playerHeight = 10 and (onPlatform or onWall or onMovingPlatform) then %squished ball
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 2, ballRadius - 3, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius - 3, ballRadius + 3, ballRadius - 2, colourOutline)
    elsif playerHeight = 10 then % squishing in the air has no effect
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, first, second, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, second, third, colour2)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, third, fourth, colour1)
	Draw.FillArc (playerX + ballRadius, playerY + ballRadius, ballRadius, ballRadius, fourth, first, colour2)
	Draw.Oval (playerX + ballRadius, playerY + ballRadius, ballRadius + 1, ballRadius + 1, colourOutline)
    end if
end DrawBall

proc DrawMan
    playerX2 := playerX + playerWidth
    playerY2 := playerY + playerHeight
    Draw.FillBox (playerX + 1, playerY + 1, playerX2 - 1, playerY2 - 1, 0)
    Draw.Box (playerX - 1, playerY - 1, playerX2 + 1, playerY2 + 1, colourOutline)
    if playerHeight = 20 then
	drawfillbox (playerX - 3, playerY2 - 1, playerX2 + 3, playerY2 + 4, colour1)
	drawfillbox (playerX + 2, playerY2 + 6, playerX2 - 2, playerY2 + 8, colour1)
	drawbox (playerX - 4, playerY2 - 2, playerX2 + 4, playerY2 + 5, colourOutline)
	drawbox (playerX + 4, playerY2 + 7, playerX2 - 4, playerY2 + 8, colourOutline)
	drawfillbox (playerX, playerY2 + 4, playerX2, playerY2 + 6, colour2)
	drawfillbox (playerX + 1, playerY2 - 5, playerX + 3, playerY2 - 3, 7)
	drawfillbox (playerX2 - 1, playerY2 - 5, playerX2 - 3, playerY2 - 3, 7)
	Draw.ThickLine (playerX + 1, playerY2 - 8, playerX2 - 1, playerY2 - 8, 2, 7)
    elsif playerHeight = 10 then %crouching
	drawfillbox (playerX - 3, playerY - 3, playerX2 + 3, playerY2 + 3, colour1)
	drawfillbox (playerX, playerY + 1, playerX2, playerY2, colour2)
	drawfillbox (playerX + 3, playerY + 4, playerX2 - 3, playerY2 - 3, colour1)
    end if
end DrawMan
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

include "Include/colours.t" %color scheme selection program
include "Include/startMenu.t"

%%%%%%%%%%%%%%%%%%%%%COLLISION%%%%%%%%%%%%%%%%%%
%Every procedure checks if the player is inside or beside the structure and then moves the player so that he is on top of the structure
%(in the case of a platform) or beside it (in the case of a wall). There is also a flag that changes the structure's color.

proc checkPlatformCollision
    for i : 1 .. upper (platforms)
	if playerX2 >= platforms (i).left and playerX <= platforms (i).right and playerY2 <= platforms (i).top + playerHeight + 1
		and playerY >= platforms (i).bottom and playerYVelocity <= 0 then
	    playerY := platforms (i).top + 1
	    playerYVelocity := 0 %stopps the player from falling through the structure
	    unlocked (i) := true
	else
	    unlocked (i) := false
	end if
    end for
    %this checks for what to light up
    for i : 1 .. upper (unlocked)
	if unlocked (i) then
	    onPlatform := true
	else
	    onPlatform := false
	end if
	exit when onPlatform
    end for
end checkPlatformCollision

proc checkOnEndPoint
    if playerX2 >= endPoint.left - 0 and playerX <= endPoint.right + 0 and playerY2 <= endPoint.top + playerHeight + 1 and playerY >= endPoint.bottom
	    and playerYVelocity <= 0 then
	playerY := endPoint.top + 1
	playerYVelocity := 0
	canHasLevelUp := true % this is a flag that advances the game to the next level
    else
	canHasLevelUp := false
    end if
end checkOnEndPoint

proc checkMovingPCollision
    for i : 1 .. upper (movingPlatform)
	if playerX2 >= movingPlatform (i).left and playerX <= movingPlatform (i).right and playerY2 <= movingPlatform (i).top + 21 + abs (movingPlatform (i).yVelocity)
		and playerY >= movingPlatform (i).bottom - abs (movingPlatform (i).yVelocity) and playerYVelocity <= 0 then
	    playerY := movingPlatform (i).top + 1
	    playerYVelocity := 0
	    unlockedM (i) := true
	else
	    unlockedM (i) := false
	end if
    end for
    for i : 1 .. upper (unlockedM)
	if unlockedM (i) then
	    onMovingPlatform := true
	else
	    onMovingPlatform := false
	end if
	exit when onMovingPlatform
    end for
end checkMovingPCollision

proc checkWallCollision
    for i : 1 .. upper (walls)

	if playerY2 <= walls (i).top and playerY >= walls (i).bottom then
	    if playerX2 <= walls (i).right and playerX2 >= walls (i).left then
		scroll := false % cannot move into wall
		%%%%% moves the player out of a wall
		for j : 1 .. upper (platforms)
		    platforms (j).left += ScrollValue
		    platforms (j).right += ScrollValue
		end for
		for j : 1 .. upper (movingPlatform)
		    movingPlatform (j).left += ScrollValue
		    movingPlatform (j).right += ScrollValue
		    movingPlatform (j).leftBound += ScrollValue
		    movingPlatform (j).rightBound += ScrollValue
		end for
		for j : 1 .. upper (walls)
		    walls (j).left += ScrollValue
		    walls (j).right += ScrollValue
		end for
		endPoint.left += ScrollValue
		endPoint.right += ScrollValue
		if choice = "ball" then
		    first += rollValue
		    second += rollValue
		    third += rollValue
		    fourth += rollValue
		end if
		%%%%%%%
	    elsif playerX <= walls (i).right + 2 and playerX >= walls (i).left then
		scroll := false
		for j : 1 .. upper (platforms)
		    platforms (j).left -= ScrollValue
		    platforms (j).right -= ScrollValue
		end for
		for j : 1 .. upper (movingPlatform)
		    movingPlatform (j).left -= ScrollValue
		    movingPlatform (j).right -= ScrollValue
		    movingPlatform (j).leftBound -= ScrollValue
		    movingPlatform (j).rightBound -= ScrollValue
		end for
		for j : 1 .. upper (walls)
		    walls (j).left -= ScrollValue
		    walls (j).right -= ScrollValue
		end for
		endPoint.left -= ScrollValue
		endPoint.right -= ScrollValue
		if choice = "ball" then
		    first -= rollValue
		    second -= rollValue
		    third -= rollValue
		    fourth -= rollValue
		end if
	    else
		scroll := true
	    end if
	end if
	if playerX2 >= walls (i).left and playerX <= walls (i).right and playerY2 <= walls (i).top + playerHeight + 1
		and playerY >= walls (i).top - 10 and playerYVelocity <= 0 then
	    playerY := walls (i).top + 1
	    playerYVelocity := 0
	    unlockedW (i) := true
	elsif playerX2 >= walls (i).left and playerX <= walls (i).right and playerY2 <= walls (i).top
		and playerY >= walls (i).bottom - playerHeight - 5 and playerYVelocity > 0 then
	    playerY := walls (i).bottom - playerHeight - 1
	    playerYVelocity := 0
	else
	    unlockedW (i) := false
	end if
    end for
    for i : 1 .. upper (unlockedW)
	if unlockedW (i) then
	    onWall := true
	else
	    onWall := false
	end if
	exit when onWall
    end for
end checkWallCollision
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%PLATFORM MOVEMENT%%%%%%%%%%%%%%%%%
proc platformMovement
    for i : 1 .. upper (movingPlatform)
	%%%%%%%%%platform stays within its given territory
	if movingPlatform (i).left <= movingPlatform (i).leftBound and movingPlatform (i).xVelocity < 0
		or movingPlatform (i).right >= movingPlatform (i).rightBound and movingPlatform (i).xVelocity > 0 then
	    movingPlatform (i).xVelocity := -movingPlatform (i).xVelocity
	end if
	if movingPlatform (i).top >= movingPlatform (i).topBound and movingPlatform (i).yVelocity > 0
		or movingPlatform (i).bottom <= movingPlatform (i).bottomBound and movingPlatform (i).yVelocity < 0
		then
	    movingPlatform (i).yVelocity := -movingPlatform (i).yVelocity
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%vertical movement
	movingPlatform (i).top += movingPlatform (i).yVelocity
	movingPlatform (i).bottom += movingPlatform (i).yVelocity
	%%%
	if unlockedM (i) = false then %if the player is not on the platform, horizontal movement
	    movingPlatform (i).left += movingPlatform (i).xVelocity
	    movingPlatform (i).right += movingPlatform (i).xVelocity
	else % if he is, everything else moves
	    for j : 1 .. upper (platforms)
		platforms (j).left -= movingPlatform (i).xVelocity
		platforms (j).right -= movingPlatform (i).xVelocity
	    end for
	    for j : 1 .. upper (movingPlatform)
		if j = i then
		    movingPlatform (j).leftBound -= movingPlatform (i).xVelocity
		    movingPlatform (j).rightBound -= movingPlatform (i).xVelocity
		else
		    movingPlatform (j).left -= movingPlatform (i).xVelocity
		    movingPlatform (j).right -= movingPlatform (i).xVelocity
		    movingPlatform (j).leftBound -= movingPlatform (i).xVelocity
		    movingPlatform (j).rightBound -= movingPlatform (i).xVelocity
		end if
	    end for
	    for j : 1 .. upper (walls)
		walls (j).left -= movingPlatform (i).xVelocity
		walls (j).right -= movingPlatform (i).xVelocity
	    end for
	    endPoint.left -= movingPlatform (i).xVelocity
	    endPoint.right -= movingPlatform (i).xVelocity
	end if
    end for
end platformMovement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%USER INPUT%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc Buttons
    Input.KeyDown (chars)
    if chars (KEY_DOWN_ARROW) or chars ('s') then %crouching, falling through ground
	playerHeight := 10
	if onPlatform then
	    squatCount += 1
	end if
	if onMovingPlatform then
	    squatCount += 1
	end if
    end if
    if (chars (KEY_UP_ARROW) or chars ('w')) and (onPlatform or onWall or onMovingPlatform) then % must be on a structure to jump
	playerYVelocity += 20
    end if
    if chars (KEY_LEFT_ARROW) or chars ('a') and scroll = true then %everything moves rather than the player
	for i : 1 .. upper (platforms)
	    platforms (i).left += ScrollValue
	    platforms (i).right += ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left += ScrollValue
	    movingPlatform (i).right += ScrollValue
	    movingPlatform (i).leftBound += ScrollValue
	    movingPlatform (i).rightBound += ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left += ScrollValue
	    walls (i).right += ScrollValue
	end for
	endPoint.left += ScrollValue
	endPoint.right += ScrollValue
	first += rollValue
	second += rollValue
	third += rollValue
	fourth += rollValue
    end if
    if chars (KEY_RIGHT_ARROW) or chars ('d') and scroll = true then
	for i : 1 .. upper (platforms)
	    platforms (i).left -= ScrollValue
	    platforms (i).right -= ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left -= ScrollValue
	    movingPlatform (i).right -= ScrollValue
	    movingPlatform (i).leftBound -= ScrollValue
	    movingPlatform (i).rightBound -= ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left -= ScrollValue
	    walls (i).right -= ScrollValue
	end for
	endPoint.left -= ScrollValue
	endPoint.right -= ScrollValue
	first -= rollValue
	second -= rollValue
	third -= rollValue
	fourth -= rollValue
    end if
    if chars (KEY_ESC) then
	goToMenu := true     %exit game
    elsif chars ('r') then
	Restart := true
    end if
end Buttons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%DRAW STRUCTURES%%%%%%%%%%%%%%%%%%%%
proc drawPlatform
    for i : 1 .. upper (platforms)
	if unlocked (i) = false then %if the player is not on the platform
	    colorbase := 17
	else %if the player is on the platform
	    colorbase := colourPlatforms
	end if
	%draw the platform
	Draw.FillBox (platforms (i).left + 1, platforms (i).bottom + 1, platforms (i).right - 1, platforms (i).top - 1, colorbase)
	Draw.Box (platforms (i).left, platforms (i).bottom, platforms (i).right, platforms (i).top, colourBorder)
    end for
end drawPlatform

proc drawMovingPlatform
    for i : 1 .. upper (movingPlatform)
	if unlockedM (i) = false then
	    colorbase := 17
	else
	    colorbase := colourPlatforms
	end if
	Draw.FillBox (movingPlatform (i).left + 1, movingPlatform (i).bottom + 1, movingPlatform (i).right - 1, movingPlatform (i).top - 1, colorbase)
	Draw.Box (movingPlatform (i).left, movingPlatform (i).bottom, movingPlatform (i).right, movingPlatform (i).top, colourBorder)
    end for
end drawMovingPlatform

proc drawWall
    for i : 1 .. upper (walls)
	if unlockedW (i) = false then
	    colorbase := 17
	else
	    colorbase := colourPlatforms
	end if
	Draw.FillBox (walls (i).left + 1, walls (i).bottom + 1, walls (i).right - 1, walls (i).top - 1, colorbase)
	Draw.Box (walls (i).left, walls (i).bottom, walls (i).right, walls (i).top, colourBorder)
    end for
end drawWall

proc drawEndPoint
    if canHasLevelUp then
	onWinPlatformCount += 1
	colorbase := brightgreen
    else
	onWinPlatformCount := 0
	colorbase := 17
    end if
    Draw.FillBox (endPoint.left + 1, endPoint.bottom + 1, endPoint.right - 1, endPoint.top - 1, colorbase)
    Draw.Box (endPoint.left, endPoint.bottom, endPoint.right, endPoint.top, colourBorder)
end drawEndPoint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%MOVESCREEN%%%%%%%%%%%%
%for moving above maxy
proc MoveScreenUp % everything moves down a screen's worth, showing one screen above
    for i : 1 .. upper (platforms)
	platforms (i).top -= maxy
	platforms (i).bottom -= maxy
    end for
    for i : 1 .. upper (walls)
	walls (i).top -= maxy
	walls (i).bottom -= maxy
    end for
    for i : 1 .. upper (movingPlatform)
	movingPlatform (i).top -= maxy
	movingPlatform (i).bottom -= maxy
	movingPlatform (i).topBound -= maxy
	movingPlatform (i).bottomBound -= maxy
    end for
    endPoint.top -= maxy
    endPoint.bottom -= maxy
    playerY := 1
end MoveScreenUp
%for moving below 0
proc MoveScreenDown
    for i : 1 .. upper (platforms)
	platforms (i).top += maxy
	platforms (i).bottom += maxy
    end for
    for i : 1 .. upper (walls)
	walls (i).top += maxy
	walls (i).bottom += maxy
    end for
    for i : 1 .. upper (movingPlatform)
	movingPlatform (i).top += maxy
	movingPlatform (i).bottom += maxy
	movingPlatform (i).topBound += maxy
	movingPlatform (i).bottomBound += maxy
    end for
    endPoint.top += maxy
    endPoint.bottom += maxy
    playerY := maxy
end MoveScreenDown

%%%%%%%%%%%%%%%%%Character Updating%%%%%%%%%%%%%%%
proc Jumping
    % gravity pulls if in the air
    if onPlatform = false then
	playerYVelocity -= gravity
    end if
    %if player falls below screen, show area below it
    if playerY < 1 then
	MoveScreenDown
    end if
    %like wise for above
    if playerY > maxy then
	MoveScreenUp
    end if
    %maximum fall rate
    if playerYVelocity < -12 then
	playerYVelocity := -12
	%drop below a platform
    elsif squatCount = 20 then
	if onMovingPlatform = true or onPlatform = true then
	    playerY -= 25
	end if
	squatCount := 0
	%squatcount resets player isn't squatting
    elsif playerHeight = 20 then
	squatCount := 0
    end if
    playerY += round (playerYVelocity)

    if choice = "man" then
	playerWidth := 10
    else
	playerWidth := 20
    end if
    playerX2 := playerX + playerWidth
    playerY2 := playerY + playerHeight
end Jumping
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc ResetLevel %reset everything within the level
    playerY := 1
    if choice = "man" then
	playerWidth := 10
    else
	playerWidth := 20
    end if
    playerX := maxx div 2 - playerWidth div 2
    playerHeight := 20
    playerYVelocity := 0
    squatCount := 0
    onWinPlatformCount := 0
    onPlatform := false
    onWall := false
    onMovingPlatform := false
    canHasLevelUp := false
    scroll := true
    Restart := false
    first := 0
    second := 90
    third := 180
    fourth := 270
end ResetLevel


%%%%%%%%%%%%%%%%%%%MAIN PROGRAM%%%%%%%%%%
choice := "ball"
loop
    Music.PlayFileLoop ("Music/Blizzard.mp3")
    loop
	startMenu
	exit when startNewGame or resumeGame or exitGame or levelSelected
    end loop
    if levelSelected then
	levelNo := selectedLevel
	GetLevels
	ResetLevel
	levelSelected := false
    end if
    if exitGame then
	Music.PlayFileStop
	exit
    end if
    if startNewGame then
	levelNo := 1
	GetLevels
	ResetLevel
    end if
    Music.PlayFileStop
    Music.PlayFileLoop ("Music/Session_1.mp3")
    loop
	if levelUp = true then
	    GetLevels
	    playerY := walls (1).top + 1
	    levelUp := false
	end if
	colorback (colourBackground)
	loop
	    cls
	    playerHeight := 20
	    platformMovement
	    Buttons
	    checkPlatformCollision
	    checkMovingPCollision
	    checkWallCollision
	    checkOnEndPoint
	    Jumping
	    checkPlatformCollision
	    checkMovingPCollision
	    checkWallCollision
	    checkOnEndPoint
	    drawPlatform
	    drawMovingPlatform
	    drawWall
	    drawEndPoint

	    if choice = "ball" then
		DrawBall
	    elsif choice = "man" then
		DrawMan
	    end if

	    Input.Flush
	    View.Update
	    Time.DelaySinceLast (15)
	    if onWinPlatformCount = 20 then
		if levelNo = YouWinLevel then
		    gameWon := true
		else
		    levelNo += 1
		    levelUp := true
		end if
		exit
	    end if
	    if goToMenu or Restart then
		exit
	    end if
	end loop
	cls
	if gameWon then
	    startNewGame := false
	    resumeGame := false
	    goToHelp := false
	    gameFirstStartup := false
	    gameWon := false
	    gameWon2 := true
	    exit
	end if
	if Restart then
	    ResetLevel
	    GetLevels
	elsif goToMenu then
	    %reset level data
	    startNewGame := false
	    resumeGame := false
	    goToHelp := false
	    playerYStoreValue := playerY
	    exit
	end if
    end loop
    Music.PlayFileStop
    goToMenu := false
end loop
Music.PlayFileStop
Window.Close (iWindow)
>>>>>>> Added Content
