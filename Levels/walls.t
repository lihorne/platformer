if levelNo = 1 then
    new walls, 7
    new unlockedW, upper (walls)
    for i : 1 .. upper (unlockedW)
	unlockedW (i) := false
    end for

    walls (1).left := -2100
    walls (1).right := 1268
    walls (1).bottom := 0
    walls (1).top := 10

    walls (2).left := -2130
    walls (2).right := -2100
    walls (2).bottom := 0
    walls (2).top := 970

    walls (3).left := 1268
    walls (3).right := 1268 + 30
    walls (3).bottom := 0
    walls (3).top := 970

    walls (4).left := -320
    walls (4).right := -310
    walls (4).bottom := 10
    walls (4).top := 650

    walls (5).left := -360
    walls (5).right := -350
    walls (5).bottom := 40
    walls (5).top := 650

    walls (6).left := 1190
    walls (6).right := 1200
    walls (6).bottom := 760
    walls (6).top := 900

    walls (7).left := -2130
    walls (7).right := 1298
    walls (7).bottom := 970
    walls (7).top := 980

elsif levelNo = 2 then
    new walls, 7
    new unlockedW, upper (walls)
    for i : 1 .. upper (unlockedW)
	unlockedW (i) := false
    end for

    walls (1).left := 0
    walls (1).right := 2200
    walls (1).bottom := 0
    walls (1).top := 10

    walls (2).left := -30
    walls (2).right := 0
    walls (2).bottom := 0
    walls (2).top := 1400

    walls (3).left := 2200
    walls (3).right := 2230
    walls (3).bottom := 0
    walls (3).top := 1400

    walls (4).left := 300
    walls (4).right := 320
    walls (4).bottom := 50
    walls (4).top := 1050

    walls (5).left := 1080
    walls (5).right := 1100
    walls (5).bottom := 10
    walls (5).top := 1050

    walls (6).left := 1180
    walls (6).right := 1200
    walls (6).bottom := 440
    walls (6).top := 1400

    walls (7).left := -30
    walls (7).right := 2230
    walls (7).bottom := 1400
    walls (7).top := 1420

elsif levelNo = 3 then
    new walls, 12
    new unlockedW, upper (walls)
    for i : 1 .. upper (unlockedW)
	unlockedW (i) := false
    end for

    walls (1).left := 0
    walls (1).right := 1080
    walls (1).bottom := 0
    walls (1).top := 10

    walls (2).left := 1200
    walls (2).right := 2300
    walls (2).bottom := 0
    walls (2).top := 10

    walls (3).left := 2370
    walls (3).right := 2600
    walls (3).bottom := 0
    walls (3).top := 10

    walls (4).left := 0
    walls (4).right := 2600
    walls (4).bottom := -700
    walls (4).top := -690

    walls (5).left := -30
    walls (5).right := 0
    walls (5).bottom := -700
    walls (5).top := 1400

    walls (6).left := 2600
    walls (6).right := 2630
    walls (6).bottom := -700
    walls (6).top := 1400

    walls (7).left := 700
    walls (7).right := 710
    walls (7).bottom := 40
    walls (7).top := 540

    walls (8).left := 1080
    walls (8).right := 1100
    walls (8).bottom := 0
    walls (8).top := 1080

    walls (9).left := 1180
    walls (9).right := 1200
    walls (9).bottom := 0
    walls (9).top := 1400

    walls (10).left := -30
    walls (10).right := 2630
    walls (10).bottom := 1400
    walls (10).top := 1420

    walls (11).left := 1030
    walls (11).right := 1040
    walls (11).bottom := -650
    walls (11).top := -250

    walls (12).left := 1250
    walls (12).right := 1260
    walls (12).bottom := -690
    walls (12).top := -250

elsif levelNo = 4 then
    new walls, 14
    new unlockedW, upper (walls)
    for i : 1 .. upper (unlockedW)
	unlockedW (i) := false
    end for

    walls (1).left := 0
    walls (1).right := 2980
    walls (1).bottom := 0
    walls (1).top := 10

    walls (2).left := -30
    walls (2).right := 0
    walls (2).bottom := -900
    walls (2).top := 900

    walls (3).left := 3015
    walls (3).right := 3045
    walls (3).bottom := -400
    walls (3).top := 900

    walls (4).left := -30
    walls (4).right := 3045
    walls (4).bottom := 900
    walls (4).top := 910

    walls (5).left := 1990
    walls (5).right := 3045
    walls (5).bottom := -410
    walls (5).top := -400

    walls (6).left := 1990
    walls (6).right := 2000
    walls (6).bottom := -890
    walls (6).top := -410

    walls (7).left := 0
    walls (7).right := 2000
    walls (7).bottom := -900
    walls (7).top := -890

    walls (8).left := 2970
    walls (8).right := 2980
    walls (8).bottom := 10
    walls (8).top := 260

    walls (9).left := 2900
    walls (9).right := 2910
    walls (9).top := 800
    walls (9).bottom := 250

    walls (10).left := 990
    walls (10).right := 1000
    walls (10).bottom := 10
    walls (10).top := 550

    walls (11).left := 1400
    walls (11).right := 1410
    walls (11).bottom := 300
    walls (11).top := 650

    walls (12).left := 1850
    walls (12).right := 1860
    walls (12).bottom := 300
    walls (12).top := 650

    walls (13).left := 40
    walls (13).right := 50
    walls (13).bottom := -160
    walls (13).top := -100
    
    walls (14).left := 100
    walls (14).right := 110
    walls (14).bottom := -160
    walls (14).top := -100
    
elsif levelNo = 5 then
    new walls, 12
    new unlockedW, upper (walls)
    for i : 1 .. upper (unlockedW)
	unlockedW (i) := false
    end for

    walls (1).left := -15
    walls (1).right := 1268 + 15
    walls (1).bottom := 0
    walls (1).top := 10

    walls (2).left := 1268
    walls (2).right := 1268 + 15
    walls (2).bottom := 0
    walls (2).top := 956

    walls (3).left := -15
    walls (3).right := 0
    walls (3).bottom := 0
    walls (3).top := 956

    walls (4).left := 115
    walls (4).right := 140
    walls (4).bottom := 520
    walls (4).top := 700

    walls (5).left := 250
    walls (5).right := 270
    walls (5).bottom := 540
    walls (5).top := 740

    walls (6).left := 410
    walls (6).right := 430
    walls (6).bottom := 520
    walls (6).top := 720

    walls (7).left := 490
    walls (7).right := 510
    walls (7).bottom := 520
    walls (7).top := 770

    walls (8).left := 650
    walls (8).right := 670
    walls (8).bottom := 520
    walls (8).top := 770

    walls (9).left := 800
    walls (9).right := 820
    walls (9).top := 410
    walls (9).bottom := 140

    walls (10).left := 790
    walls (10).right := 830
    walls (10).bottom := 420
    walls (10).top := 460

    walls (11).left := 910
    walls (11).right := 935
    walls (11).bottom := 140
    walls (11).top := 450

    walls (12).left := 1060
    walls (12).right := 1085
    walls (12).bottom := 140
    walls (12).top := 450

end if
