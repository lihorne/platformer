if levelNo = 1 then
    new platforms, 22
    new unlocked, upper (platforms)
    for i : 1 .. upper (unlocked)
	unlocked (i) := false
    end for

    platforms (1).left := 100
    platforms (1).right := 250
    platforms (1).bottom := 140
    platforms (1).top := 150

    platforms (2).left := 500
    platforms (2).right := 650
    platforms (2).bottom := 255
    platforms (2).top := 265

    platforms (3).left := 900
    platforms (3).right := 1050
    platforms (3).bottom := 390
    platforms (3).top := 400

    platforms (4).left := 400
    platforms (4).right := 1268
    platforms (4).bottom := 500
    platforms (4).top := 510

    platforms (5).left := -250
    platforms (5).right := -200
    platforms (5).bottom := 530
    platforms (5).top := 540

    platforms (6).left := -360
    platforms (6).right := -310
    platforms (6).bottom := 650
    platforms (6).top := 660

    platforms (7).left := -920
    platforms (7).right := -650
    platforms (7).bottom := 660
    platforms (7).top := 670

    platforms (8).left := -1050
    platforms (8).right := -1040
    platforms (8).bottom := 600
    platforms (8).top := 610

    platforms (9).left := -1180
    platforms (9).right := -1170
    platforms (9).bottom := 540
    platforms (9).top := 550

    platforms (10).left := -1310
    platforms (10).right := -1300
    platforms (10).bottom := 480
    platforms (10).top := 490

    platforms (11).left := -1440
    platforms (11).right := -1430
    platforms (11).bottom := 420
    platforms (11).top := 430

    platforms (12).left := -1570
    platforms (12).right := -1560
    platforms (12).bottom := 360
    platforms (12).top := 370

    platforms (13).left := -1900
    platforms (13).right := -1690
    platforms (13).bottom := 230
    platforms (13).top := 240

    platforms (14).left := -1900
    platforms (14).right := -1800
    platforms (14).bottom := 330
    platforms (14).top := 340

    platforms (15).left := -1790
    platforms (15).right := -1690
    platforms (15).bottom := 450
    platforms (15).top := 460

    platforms (16).left := -1900
    platforms (16).right := -1800
    platforms (16).bottom := 550
    platforms (16).top := 560

    platforms (17).left := -1790
    platforms (17).right := -1690
    platforms (17).bottom := 670
    platforms (17).top := 680

    platforms (18).left := -2100
    platforms (18).right := -1900
    platforms (18).bottom := 790
    platforms (18).top := 800

    platforms (19).left := -1900
    platforms (19).right := -300
    platforms (19).bottom := 890
    platforms (19).top := 900

    platforms (20).left := -275
    platforms (20).right := -265
    platforms (20).bottom := 890
    platforms (20).top := 900

    platforms (21).left := -240
    platforms (21).right := 0
    platforms (21).bottom := 890
    platforms (21).top := 900

    platforms (22).left := 70
    platforms (22).right := 1200
    platforms (22).bottom := 900
    platforms (22).top := 910

    endPoint.left := 1200
    endPoint.right := 1268
    endPoint.bottom := 760
    endPoint.top := 770

elsif levelNo = 2 then
    new platforms, 14
    new unlocked, upper (platforms)
    for i : 1 .. upper (unlocked)
	unlocked (i) := false
    end for

    platforms (1).left := 0
    platforms (1).right := 50
    platforms (1).bottom := 140
    platforms (1).top := 150

    platforms (2).left := 250
    platforms (2).right := 300
    platforms (2).bottom := 290
    platforms (2).top := 300

    platforms (3).left := 0
    platforms (3).right := 50
    platforms (3).bottom := 440
    platforms (3).top := 450

    platforms (4).left := 250
    platforms (4).right := 300
    platforms (4).bottom := 590
    platforms (4).top := 600

    platforms (5).left := 0
    platforms (5).right := 50
    platforms (5).bottom := 740
    platforms (5).top := 750

    platforms (6).left := 250
    platforms (6).right := 300
    platforms (6).bottom := 890
    platforms (6).top := 900

    platforms (7).left := 0
    platforms (7).right := 50
    platforms (7).bottom := 1040
    platforms (7).top := 1050

    platforms (8).left := 320
    platforms (8).right := 370
    platforms (8).bottom := 1040
    platforms (8).top := 1050

    platforms (9).left := 1100
    platforms (9).right := 1180
    platforms (9).bottom := 1040
    platforms (9).top := 1050

    platforms (10).left := 1100
    platforms (10).right := 1180
    platforms (10).bottom := 840
    platforms (10).top := 850

    platforms (11).left := 1100
    platforms (11).right := 1180
    platforms (11).bottom := 640
    platforms (11).top := 650

    platforms (12).left := 1100
    platforms (12).right := 1180
    platforms (12).bottom := 440
    platforms (12).top := 450

    platforms (13).left := 1100
    platforms (13).right := 1180
    platforms (13).bottom := 140
    platforms (13).top := 150

    platforms (14).left := 1700
    platforms (14).right := 1800
    platforms (14).bottom := 1100
    platforms (14).top := 1110

    endPoint.left := 2100
    endPoint.right := 2200
    endPoint.bottom := 1100
    endPoint.top := 1110

elsif levelNo = 3 then
    new platforms, 18
    new unlocked, upper (platforms)
    for i : 1 .. upper (unlocked)
	unlocked (i) := false
    end for

    platforms (1).left := 0
    platforms (1).right := 700
    platforms (1).bottom := 530
    platforms (1).top := 540

    platforms (2).left := 270
    platforms (2).right := 430
    platforms (2).bottom := 360
    platforms (2).top := 370

    platforms (3).left := 270
    platforms (3).right := 430
    platforms (3).bottom := 160
    platforms (3).top := 170

    platforms (4).left := 590
    platforms (4).right := 650
    platforms (4).bottom := 630
    platforms (4).top := 640

    platforms (5).left := 370
    platforms (5).right := 430
    platforms (5).bottom := 760
    platforms (5).top := 770

    platforms (6).left := 170
    platforms (6).right := 230
    platforms (6).bottom := 860
    platforms (6).top := 870

    platforms (7).left := 0
    platforms (7).right := 60
    platforms (7).bottom := 960
    platforms (7).top := 970

    platforms (8).left := 1020
    platforms (8).right := 1080
    platforms (8).bottom := 1070
    platforms (8).top := 1080

    platforms (9).left := 0
    platforms (9).right := 100
    platforms (9).bottom := -520
    platforms (9).top := -510

    platforms (10).left := 0
    platforms (10).right := 100
    platforms (10).bottom := -340
    platforms (10).top := -330

    platforms (11).left := 250
    platforms (11).right := 1040
    platforms (11).bottom := -250
    platforms (11).top := -240

    platforms (12).left := 1250
    platforms (12).right := 2000
    platforms (12).bottom := -250
    platforms (12).top := -240

    platforms (13).left := 2100
    platforms (13).right := 2180
    platforms (13).bottom := 340
    platforms (13).top := 350

    platforms (14).left := 2100
    platforms (14).right := 2180
    platforms (14).bottom := 480
    platforms (14).top := 490

    platforms (15).left := 2300
    platforms (15).right := 2380
    platforms (15).bottom := 540
    platforms (15).top := 550

    platforms (16).left := 2300
    platforms (16).right := 2380
    platforms (16).bottom := 690
    platforms (16).top := 700

    platforms (17).left := 2100
    platforms (17).right := 2180
    platforms (17).bottom := 820
    platforms (17).top := 830

    platforms (18).left := 1600
    platforms (18).right := 1680
    platforms (18).bottom := 700
    platforms (18).top := 710

    endPoint.left := 1450
    endPoint.right := 1500
    endPoint.bottom := 850
    endPoint.top := 860

elsif levelNo = 4 then
    new platforms, 32
    new unlocked, upper (platforms)
    for i : 1 .. upper (unlocked)
	unlocked (i) := false
    end for

    platforms (1).left := 350
    platforms (1).right := 500
    platforms (1).bottom := 140
    platforms (1).top := 150

    platforms (2).left := 50
    platforms (2).right := 200
    platforms (2).bottom := 250
    platforms (2).top := 260

    platforms (3).left := 300
    platforms (3).right := 450
    platforms (3).bottom := 400
    platforms (3).top := 410

    platforms (4).left := 990
    platforms (4).right := 1400
    platforms (4).bottom := 550
    platforms (4).top := 560

    platforms (5).left := 1850
    platforms (5).right := 2100
    platforms (5).bottom := 550
    platforms (5).top := 560

    platforms (6).left := 1360
    platforms (6).right := 1410
    platforms (6).bottom := 650
    platforms (6).top := 660

    platforms (7).left := 1850
    platforms (7).right := 1900
    platforms (7).bottom := 650
    platforms (7).top := 660

    platforms (8).left := 1590
    platforms (8).right := 1610
    platforms (8).bottom := 650
    platforms (8).top := 660

    platforms (9).left := 1100
    platforms (9).right := 1170
    platforms (9).bottom := 140
    platforms (9).top := 150

    platforms (10).left := 1280
    platforms (10).right := 1350
    platforms (10).bottom := 280
    platforms (10).top := 290

    platforms (11).left := 1100
    platforms (11).right := 1170
    platforms (11).bottom := 420
    platforms (11).top := 430

    platforms (12).left := 1410
    platforms (12).right := 1850
    platforms (12).bottom := 300
    platforms (12).top := 310

    platforms (13).left := 2500
    platforms (13).right := 2550
    platforms (13).bottom := 400
    platforms (13).top := 410

    platforms (14).left := 2850
    platforms (14).right := 2900
    platforms (14).bottom :=350
    platforms (14).top := 360

    platforms (15).left := 2850
    platforms (15).right := 2900
    platforms (15).bottom := 550
    platforms (15).top := 560

    platforms (16).left := 2670
    platforms (16).right := 2750
    platforms (16).bottom := 850
    platforms (16).top := 860

    platforms (17).left := 2520
    platforms (17).right := 2570
    platforms (17).bottom := 620
    platforms (17).top := 630

    platforms (18).left := 2550
    platforms (18).right := 2600
    platforms (18).bottom := 800
    platforms (18).top := 810

    platforms (19).left := 2870
    platforms (19).right := 2910
    platforms (19).bottom := 800
    platforms (19).top := 810

    platforms (20).left := 2910
    platforms (20).right := 2970
    platforms (20).bottom := 250
    platforms (20).top := 260

    platforms (21).left := 1700
    platforms (21).right := 1770
    platforms (21).bottom := -460
    platforms (21).top := -450

    platforms (22).left := 1350
    platforms (22).right := 1400
    platforms (22).bottom := -410
    platforms (22).top := -400

    platforms (23).left := 1100
    platforms (23).right := 1150
    platforms (23).bottom := -260
    platforms (23).top := -250
    
    platforms (24).left := 1150
    platforms (24).right := 1200
    platforms (24).bottom := -510
    platforms (24).top := -500
    
    platforms (25).left := 1150
    platforms (25).right := 1200
    platforms (25).bottom := -660
    platforms (25).top := -650
    
    platforms (26).left := 1350
    platforms (26).right := 1400
    platforms (26).bottom := -760
    platforms (26).top := -750
    
    platforms (27).left := 850
    platforms (27).right := 900
    platforms (27).bottom := -260
    platforms (27).top := -250
    
    platforms (28).left := 370
    platforms (28).right := 420
    platforms (28).bottom := -200
    platforms (28).top := -190
    
    platforms (29).left := 600
    platforms (29).right := 650
    platforms (29).bottom := -510
    platforms (29).top := -500
   
    platforms (30).left := 330
    platforms (30).right := 380
    platforms (30).bottom := -360
    platforms (30).top := -350
    
    platforms (31).left := 200
    platforms (31).right := 250
    platforms (31).bottom := -90
    platforms (31).top := -80
   
    platforms (32).left := 50
    platforms (32).right := 100
    platforms (32).bottom := -110
    platforms (32).top := -100
    
    endPoint.left := 50
    endPoint.right := 100
    endPoint.bottom := -160
    endPoint.top := -150

elsif levelNo = 5 then
    new platforms, 3
    new unlocked, upper (platforms)
    for i : 1 .. upper (unlocked)
	unlocked (i) := false
    end for

    platforms (1).left := 250
    platforms (1).right := platforms (1).left + 160     %=410
    platforms (1).bottom := 520
    platforms (1).top := 540

    platforms (2).left := platforms (1).left + 20       %=270
    platforms (2).right := platforms (1).right + 20     %=430
    platforms (2).bottom := platforms (1).bottom + 200  %=720
    platforms (2).top := platforms (1).top + 200        %=740

    platforms (3).left := platforms (1).right + 100     %=510
    platforms (3).right := platforms (3).left + 140     %=650
    platforms (3).bottom := platforms (1).bottom        %=520
    platforms (3).top := platforms (1).top              %=540

    endPoint.left := -15
    endPoint.right := 1268 + 15
    endPoint.bottom := 956
    endPoint.top := 956 + 15

end if
