
if levelNo = 1 then
    new movingPlatform, 2
    new unlockedM, upper (movingPlatform)
    for i : 1 .. upper (unlockedM)
	unlockedM (i) := false
    end for

    movingPlatform (1).left := -120
    movingPlatform (1).right := -40
    movingPlatform (1).bottom := 500
    movingPlatform (1).top := 510
    movingPlatform (1).leftBound := platforms (5).right + 10
    movingPlatform (1).rightBound := platforms (4).left - 5
    movingPlatform (1).topBound := 510
    movingPlatform (1).bottomBound := 500
    movingPlatform (1).xVelocity := 3
    movingPlatform (1).yVelocity := 0

    movingPlatform (2).left := -350
    movingPlatform (2).right := -320
    movingPlatform (2).bottom := 40
    movingPlatform (2).top := 50
    movingPlatform (2).leftBound := -350
    movingPlatform (2).rightBound := -320
    movingPlatform (2).topBound := 635
    movingPlatform (2).bottomBound := 40
    movingPlatform (2).xVelocity := 0
    movingPlatform (2).yVelocity := 2

elsif levelNo = 2 then
    new movingPlatform, 5
    new unlockedM, upper (movingPlatform)
    for i : 1 .. upper (unlockedM)
	unlockedM (i) := false
    end for

    movingPlatform (1).left := 370
    movingPlatform (1).right := 420
    movingPlatform (1).bottom := 1040
    movingPlatform (1).top := 1050
    movingPlatform (1).leftBound := platforms (8).right
    movingPlatform (1).rightBound := walls (5).left
    movingPlatform (1).topBound := 1050
    movingPlatform (1).bottomBound := 1040
    movingPlatform (1).xVelocity := 3
    movingPlatform (1).yVelocity := 0

    movingPlatform (2).left := 1180
    movingPlatform (2).right := 1230
    movingPlatform (2).bottom := 240
    movingPlatform (2).top := 250
    movingPlatform (2).leftBound := 1180
    movingPlatform (2).rightBound := 1620
    movingPlatform (2).topBound := 445
    movingPlatform (2).bottomBound := 240
    movingPlatform (2).xVelocity := 2
    movingPlatform (2).yVelocity := 1

    movingPlatform (3).left := 1870
    movingPlatform (3).right := 1920
    movingPlatform (3).bottom := 585
    movingPlatform (3).top := 595
    movingPlatform (3).leftBound := 1580
    movingPlatform (3).rightBound := 1920
    movingPlatform (3).topBound := 595
    movingPlatform (3).bottomBound := 440
    movingPlatform (3).xVelocity := 2
    movingPlatform (3).yVelocity := 1

    movingPlatform (4).left := 1620
    movingPlatform (4).right := 1690
    movingPlatform (4).bottom := 410
    movingPlatform (4).top := 420
    movingPlatform (4).leftBound := 1620
    movingPlatform (4).rightBound := 1820
    movingPlatform (4).topBound := 420
    movingPlatform (4).bottomBound := 410
    movingPlatform (4).xVelocity := 2
    movingPlatform (4).yVelocity := 0

    movingPlatform (5).left := 1600
    movingPlatform (5).right := 1650
    movingPlatform (5).bottom := 620
    movingPlatform (5).top := 630
    movingPlatform (5).leftBound := 1550
    movingPlatform (5).rightBound := 1800
    movingPlatform (5).topBound := 1000
    movingPlatform (5).bottomBound := 620
    movingPlatform (5).xVelocity := 2
    movingPlatform (5).yVelocity := 1

elsif levelNo = 3 then
    new movingPlatform, 4
    new unlockedM, upper (movingPlatform)
    for i : 1 .. upper (unlockedM)
	unlockedM (i) := false
    end for

    movingPlatform (1).left := 170
    movingPlatform (1).right := 230
    movingPlatform (1).bottom := 1070
    movingPlatform (1).top := 1080
    movingPlatform (1).leftBound := 170
    movingPlatform (1).rightBound := 1020
    movingPlatform (1).topBound := 1080
    movingPlatform (1).bottomBound := 1070
    movingPlatform (1).xVelocity := 3
    movingPlatform (1).yVelocity := 0

    movingPlatform (2).left := 2200
    movingPlatform (2).right := 2270
    movingPlatform (2).bottom := -250
    movingPlatform (2).top := -240
    movingPlatform (2).leftBound := 2200
    movingPlatform (2).rightBound := 2270
    movingPlatform (2).topBound := -250
    movingPlatform (2).bottomBound := -600
    movingPlatform (2).xVelocity := 0
    movingPlatform (2).yVelocity := 1

    movingPlatform (3).left := 2300
    movingPlatform (3).right := 2370
    movingPlatform (3).bottom := -250
    movingPlatform (3).top := -240
    movingPlatform (3).leftBound := 2200
    movingPlatform (3).rightBound := 2270
    movingPlatform (3).topBound := 350
    movingPlatform (3).bottomBound := -300
    movingPlatform (3).xVelocity := 0
    movingPlatform (3).yVelocity := 1

    movingPlatform (4).left := 2300
    movingPlatform (4).right := 2370
    movingPlatform (4).bottom := 340
    movingPlatform (4).top := 350
    movingPlatform (4).leftBound := 2200
    movingPlatform (4).rightBound := 2270
    movingPlatform (4).topBound := 350
    movingPlatform (4).bottomBound := -300
    movingPlatform (4).xVelocity := 0
    movingPlatform (4).yVelocity := 1

elsif levelNo = 4 then
    new movingPlatform, 1
    new unlockedM, upper (movingPlatform)
    for i : 1 .. upper (unlockedM)
	unlockedM (i) := false
    end for

    movingPlatform (1).left := 650
    movingPlatform (1).right := 700
    movingPlatform (1).bottom := 490
    movingPlatform (1).top := 500
    movingPlatform (1).leftBound := 650
    movingPlatform (1).rightBound := 700
    movingPlatform (1).topBound := 600
    movingPlatform (1).bottomBound := 400
    movingPlatform (1).xVelocity := 0
    movingPlatform (1).yVelocity := 1

elsif levelNo = 5 then
    new movingPlatform, 14
    new unlockedM, upper (movingPlatform)
    for i : 1 .. upper (unlockedM)
	unlockedM (i) := false
    end for
    %%%%the Y%%%%%%
    movingPlatform (1).left := 115
    movingPlatform (1).right := 140
    movingPlatform (1).bottom := 690
    movingPlatform (1).top := 700
    movingPlatform (1).leftBound := 115
    movingPlatform (1).rightBound := 240
    movingPlatform (1).topBound := 850
    movingPlatform (1).bottomBound := 690
    movingPlatform (1).xVelocity := 2
    movingPlatform (1).yVelocity := 3

    movingPlatform (2).left := 15
    movingPlatform (2).right := 40
    movingPlatform (2).bottom := 840
    movingPlatform (2).top := 850
    movingPlatform (2).leftBound := 15
    movingPlatform (2).rightBound := 140
    movingPlatform (2).topBound := 850
    movingPlatform (2).bottomBound := 690
    movingPlatform (2).xVelocity := 2
    movingPlatform (2).yVelocity := -3

    movingPlatform (3).left := 115
    movingPlatform (3).right := 140
    movingPlatform (3).bottom := 690
    movingPlatform (3).top := 700
    movingPlatform (3).leftBound := 15
    movingPlatform (3).rightBound := 140
    movingPlatform (3).topBound := 850
    movingPlatform (3).bottomBound := 690
    movingPlatform (3).xVelocity := -2
    movingPlatform (3).yVelocity := 3

    movingPlatform (4).left := 215
    movingPlatform (4).right := 240
    movingPlatform (4).bottom := 840
    movingPlatform (4).top := 850
    movingPlatform (4).leftBound := 115
    movingPlatform (4).rightBound := 240
    movingPlatform (4).topBound := 850
    movingPlatform (4).bottomBound := 690
    movingPlatform (4).xVelocity := -2
    movingPlatform (4).yVelocity := -3
    %%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%the N%%%%%%%%%%%%%%%
    movingPlatform (5).left := 910
    movingPlatform (5).right := 935
    movingPlatform (5).bottom := 440
    movingPlatform (5).top := 450
    movingPlatform (5).leftBound := 910
    movingPlatform (5).rightBound := 1085
    movingPlatform (5).topBound := 450
    movingPlatform (5).bottomBound := 140
    movingPlatform (5).xVelocity := 2
    movingPlatform (5).yVelocity := -4

    movingPlatform (6).left := 1060
    movingPlatform (6).right := 1085
    movingPlatform (6).bottom := 140
    movingPlatform (6).top := 150
    movingPlatform (6).leftBound := 910
    movingPlatform (6).rightBound := 1085
    movingPlatform (6).topBound := 450
    movingPlatform (6).bottomBound := 140
    movingPlatform (6).xVelocity := -2
    movingPlatform (6).yVelocity := 4

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%The W%%%%%%%%%%%%%%
    for i : 7 .. upper (movingPlatform)
	movingPlatform (i).rightBound := walls (9).left - 50 %=750
	movingPlatform (i).leftBound := walls (9).left - 520 %=310
	movingPlatform (i).bottomBound := walls (9).bottom   %=140
	movingPlatform (i).topBound := walls (12).top + 30   %=480
    end for
    movingPlatform (7).left := movingPlatform (7).leftBound
    movingPlatform (7).right := movingPlatform (7).left + 30
    movingPlatform (7).top := movingPlatform (7).topBound
    movingPlatform (7).bottom := movingPlatform (7).top - 10
    movingPlatform (7).xVelocity := 2
    movingPlatform (7).yVelocity := 6

    movingPlatform (8).left := movingPlatform (7).rightBound - 30
    movingPlatform (8).right := movingPlatform (7).rightBound
    movingPlatform (8).top := movingPlatform (7).topBound
    movingPlatform (8).bottom := movingPlatform (7).top - 10
    movingPlatform (8).xVelocity := 2
    movingPlatform (8).yVelocity := 6

    movingPlatform (9).left := movingPlatform (7).rightBound - 140
    movingPlatform (9).right := movingPlatform (7).rightBound - 110
    movingPlatform (9).top := movingPlatform (7).bottomBound + 10
    movingPlatform (9).bottom := movingPlatform (7).bottomBound
    movingPlatform (9).xVelocity := 2
    movingPlatform (9).yVelocity := 6

    movingPlatform (10).left := movingPlatform (7).leftBound + 110
    movingPlatform (10).right := movingPlatform (7).leftBound + 140
    movingPlatform (10).top := movingPlatform (7).bottomBound + 10
    movingPlatform (10).bottom := movingPlatform (7).bottomBound
    movingPlatform (10).xVelocity := -2
    movingPlatform (10).yVelocity := 6

    movingPlatform (11).left := movingPlatform (7).leftBound + 220
    movingPlatform (11).right := movingPlatform (7).leftBound + 250
    movingPlatform (11).top := movingPlatform (7).topBound
    movingPlatform (11).bottom := movingPlatform (7).topBound - 10
    movingPlatform (11).xVelocity := -2
    movingPlatform (11).yVelocity := 6

    movingPlatform (12).left := movingPlatform (7).leftBound + 220
    movingPlatform (12).right := movingPlatform (7).leftBound + 250
    movingPlatform (12).top := movingPlatform (7).topBound
    movingPlatform (12).bottom := movingPlatform (7).topBound - 10
    movingPlatform (12).xVelocity := 2
    movingPlatform (12).yVelocity := 6

    movingPlatform (13).left := movingPlatform (7).rightBound - 140
    movingPlatform (13).right := movingPlatform (7).rightBound - 110
    movingPlatform (13).top := movingPlatform (7).bottomBound + 10
    movingPlatform (13).bottom := movingPlatform (7).bottomBound
    movingPlatform (13).xVelocity := -2
    movingPlatform (13).yVelocity := 6

    movingPlatform (14).left := movingPlatform (7).leftBound + 110
    movingPlatform (14).right := movingPlatform (7).leftBound + 140
    movingPlatform (14).top := movingPlatform (7).bottomBound + 10
    movingPlatform (14).bottom := movingPlatform (7).bottomBound
    movingPlatform (14).xVelocity := 2
    movingPlatform (14).yVelocity := 6

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end if
