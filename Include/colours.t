%%%%%%%%%%%%%%%%%%%%%%%%VARIABLES%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
var colourX, colourY : int := 0
var colourx : array 1 .. 26 of int
var coloury : array 1 .. 10 of int
var colourChoiceX : array 1 .. 26 of boolean
for i : 1 .. 26
    colourChoiceX (i) := false
end for
var colourChoiceY : array 1 .. 10 of boolean
for i : 1 .. 10
    colourChoiceY (i) := false
end for
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%SETS CO-ORDINATES FOR EACH COLOUR BUTTON%%%%%%%%%%%%%%
colourX := 655
colourY := 250
for i : 1 .. 26
    colourX += 10
    colourx (i) := colourX
    colourX += 10
end for
for i : 1 .. 10
    coloury (i) := colourY
    colourY -= 20
end for
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc ColourChoice %PROCEDURE THAT DRAWS A PALETTE OF ALL 256 COLOURS FOR THE USER TO CHOOSE FROM
    colourX := 655
    colourY := 250
    for i : 0 .. 255
	colourX += 10
	drawfillbox ((colourX), colourY, (colourX) + 10, colourY + 10, i)
	drawbox ((colourX) - 1, colourY - 1, (colourX) + 11, colourY + 11, 0)
	colourX += 10
	if colourX + 10 > 1175 then
	    colourX := 655
	    colourY -= 20
	end if
    end for
end ColourChoice

proc getColour1 %GETS THE COLOUR FOR THE MAIN COLOUR
    for i : 1 .. 26%CHECKS TO SEE WHICH OF THE 26 COLOUMNS IN THE PALETTE THE MOUSE WAS CLICKED IN
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10%CHECKS TO SEE WHICH OF THE  10 ROWS IN THE PALETTE THE MOUSE WAS CLICKED IN
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then%CROSS CHECKS THE ROW AND COLOUMN TO FIND OUT WHICH BUTTON WAS PUSHED
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colour1 := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColour1

proc getColour2 %GETS THE SECONDARY COLOUR USING THE SAME METHOD AS getColour1
    for i : 1 .. 26
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colour2 := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColour2

proc getColourOutline %GETS THE COLOUR FOR THE PLAYERS OUTLINE USING THE SAME METHOD AS getColour1
    for i : 1 .. 26
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colourOutline := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColourOutline

proc getColourPlatforms %GETS THE COLOUR FOR THE PLATFORMS USING THE SAME METHOD AS getColour1
    for i : 1 .. 26
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colourPlatforms := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColourPlatforms

proc getColourBackground %GETS THE BACKGROUND COLOUR USING THE SAME METHOD AS getColour1
    for i : 1 .. 26
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colourBackground := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColourBackground

proc getColourBorder %GETS THE PLATFORM'S BORDERCOLOUR USING THE SAME METHOD AS getColour1
    for i : 1 .. 26
	if mouseX >= colourx (i) and mouseX <= colourx (i) + 10 and mouseButton not= 0 then
	    colourChoiceX (i) := true
	end if
    end for
    for i : 1 .. 10
	if mouseY >= coloury (i) and mouseY <= coloury (i) + 10 and mouseButton not= 0 then
	    colourChoiceY (i) := true
	end if
    end for
    for i : 1 .. 10
	for j : 1 .. 26
	    if colourChoiceY (i) = true and colourChoiceX (j) = true then
		if (((i - 1) * 26) + j) - 1 <= 255 then
		    colourBorder := (((i - 1) * 26) + j) - 1
		    colourChoiceY (i) := false
		    colourChoiceX (j) := false
		end if
	    end if
	end for
    end for
end getColourBorder

proc preview %SHOWS A PREVIEW OF THE COLOUR SCHEEMS THAT HAS BEEN CHOSEN BY THE USER IN THE START MENU BEFORE THEY ENTER INTO THE MAIN GAME
    drawfillbox (674, 449, 1151, 901, colourBackground) %DRAWS A SAMPLE BACKGROUND
    drawfillbox (675, 450, 1150, 460, colourPlatforms) %DRAWS A SAMPLE LIT PLATFORM
    drawbox (674, 449, 1151, 461, colourBorder) %DRAWS A SAMPLE PLATFORM BORDER
    drawbox (675, 550, 775, 560, colourBorder) %DRAWS A SAMPLE PLATFORM BORDER
    drawfillbox (676, 551, 774, 559, 17) %DRAWS A SAMPLE PLATFORM
    drawbox (875, 650, 925, 660, colourBorder) %DRAWS A SAMPLE PLATFORM BORDER
    drawfillbox (876, 651, 924, 659, 17) %DRAWS A SAMPLE PLATFORM
    drawbox (895, 750, 1095, 760, colourBorder) %DRAWS A SAMPLE PLATFORM BORDER
    drawfillbox (896, 751, 1094, 759, 17) %DRAWS A SAMPLE PLATFORM
    if choice = "ball" then %DRAWS A SAMPLE OF THE CHOSEN CHARACTER WITH THE CHOSEN LEVEL COLOURS
	playerY := 463
	playerX := 875
	DrawBall
    elsif choice = "man" then
	playerY := 463
	playerX := 880
	DrawMan
    end if
end preview

