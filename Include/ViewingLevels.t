%%%Development aid. Not part fo the program.
var iWindow : int := Window.Open ("graphics:1268;956")
setscreen ("offscreenonly,nobuttonbar,noecho,nocursor,title:Level Builder")
var chars : array char of boolean
type barrier :
    record
	left : int
	right : int
	top : int
	bottom : int
    end record
var platforms : flexible array 1 .. 0 of barrier
var endPoint : barrier
var walls : flexible array 1 .. 0 of barrier
var unlocked : flexible array 1 .. 0 of boolean
var unlockedW : flexible array 1 .. 0 of boolean
var unlockedM : flexible array 1 .. 0 of boolean
type movingBarrier :
    record
	left, right, bottom, top : int
	leftBound, rightBound, topBound, bottomBound : int
	xVelocity, yVelocity : int
    end record
var movingPlatform : flexible array 1 .. 0 of movingBarrier
var levelNo := 4
const ScrollValue:=30
include "Levels\platforms.t"
include "Levels\walls.t"
include "Levels\movingPlatform.t"
%%%%%%%%%%%%%%%%%%%%%%%%%%PLATFFORM MOVEMENT%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc platformMovement (i : int)

    if movingPlatform (i).left <= movingPlatform (i).leftBound and movingPlatform (i).xVelocity < 0
	    or movingPlatform (i).right >= movingPlatform (i).rightBound and movingPlatform (i).xVelocity > 0 then
	movingPlatform (i).xVelocity := -movingPlatform (i).xVelocity
    end if
    if movingPlatform (i).top >= movingPlatform (i).topBound and movingPlatform (i).yVelocity > 0
	    or movingPlatform (i).bottom <= movingPlatform (i).bottomBound and movingPlatform (i).yVelocity < 0
	    then
	movingPlatform (i).yVelocity := -movingPlatform (i).yVelocity
    end if
    movingPlatform (i).top += movingPlatform (i).yVelocity
    movingPlatform (i).bottom += movingPlatform (i).yVelocity

    movingPlatform (i).left += movingPlatform (i).xVelocity
    movingPlatform (i).right += movingPlatform (i).xVelocity

end platformMovement
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%DRAW PLATFORMS%%%%%%%%%%%%%%%%%%%%
proc drawPlatform (colorbase : int, n : int)
    Draw.FillBox (platforms (n).left + 1, platforms (n).bottom + 1, platforms (n).right - 1, platforms (n).top - 1, colorbase)
    Draw.Box (platforms (n).left, platforms (n).bottom, platforms (n).right, platforms (n).top, 23)
end drawPlatform

proc drawMovingPlatform (colorbase : int, n : int)
    Draw.FillBox (movingPlatform (n).left + 1, movingPlatform (n).bottom + 1, movingPlatform (n).right - 1, movingPlatform (n).top - 1, colorbase)
    Draw.Box (movingPlatform (n).left, movingPlatform (n).bottom, movingPlatform (n).right, movingPlatform (n).top, 23)
end drawMovingPlatform

proc drawWall (colorbase : int, n : int)
    Draw.FillBox (walls (n).left + 1, walls (n).bottom + 1, walls (n).right - 1, walls (n).top - 1, colorbase)
    Draw.Box (walls (n).left, walls (n).bottom, walls (n).right, walls (n).top, 23)
end drawWall

proc drawEndPoint (colorbase : int)
    Draw.FillBox (endPoint.left + 1, endPoint.bottom + 1, endPoint.right - 1, endPoint.top - 1, colorbase)
    Draw.Box (endPoint.left, endPoint.bottom, endPoint.right, endPoint.top, 23)
end drawEndPoint
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%INPUT%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc Buttons
    Input.KeyDown (chars)
    if chars ('w') then
	for i : 1 .. upper (platforms)
	    platforms (i).top -=ScrollValue
	    platforms (i).bottom -=ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).top -=ScrollValue
	    movingPlatform (i).bottom -=ScrollValue
	    movingPlatform (i).topBound -=ScrollValue
	    movingPlatform (i).bottomBound -=ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).top -=ScrollValue
	    walls (i).bottom -=ScrollValue
	end for
	endPoint.top -=ScrollValue
	endPoint.bottom -=ScrollValue
    elsif chars ('a') then
	for i : 1 .. upper (platforms)
	    platforms (i).left +=ScrollValue
	    platforms (i).right +=ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left +=ScrollValue
	    movingPlatform (i).right +=ScrollValue
	    movingPlatform (i).leftBound +=ScrollValue
	    movingPlatform (i).rightBound +=ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left +=ScrollValue
	    walls (i).right +=ScrollValue
	end for
	endPoint.left +=ScrollValue
	endPoint.right +=ScrollValue
    elsif chars ('d') then
	for i : 1 .. upper (platforms)
	    platforms (i).left -=ScrollValue
	    platforms (i).right -=ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).left -=ScrollValue
	    movingPlatform (i).right -=ScrollValue
	    movingPlatform (i).leftBound -=ScrollValue
	    movingPlatform (i).rightBound -=ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).left -=ScrollValue
	    walls (i).right -=ScrollValue
	end for
	endPoint.left -=ScrollValue
	endPoint.right -=ScrollValue
    elsif chars ('s') then
	for i : 1 .. upper (platforms)
	    platforms (i).top +=ScrollValue
	    platforms (i).bottom +=ScrollValue
	end for
	for i : 1 .. upper (movingPlatform)
	    movingPlatform (i).top +=ScrollValue
	    movingPlatform (i).bottom +=ScrollValue
	    movingPlatform (i).topBound +=ScrollValue
	    movingPlatform (i).bottomBound +=ScrollValue
	end for
	for i : 1 .. upper (walls)
	    walls (i).top +=ScrollValue
	    walls (i).bottom +=ScrollValue
	end for
	endPoint.top +=ScrollValue
	endPoint.bottom +=ScrollValue
    end if
end Buttons
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
colorback (17)
cls
loop
    cls
    for i : 1 .. upper (platforms)
	drawPlatform (53, i)
    end for

    for i : 1 .. upper (movingPlatform)
	drawMovingPlatform (53, i)
    end for

    for i : 1 .. upper (walls)
	drawWall (53, i)
    end for
    drawEndPoint (brightgreen)
    View.Update
    delay (40)
    Buttons
    exit when chars (KEY_ESC)
    for i : 1 .. upper (movingPlatform)
	platformMovement (i)
    end for
end loop
Window.Close (iWindow)
