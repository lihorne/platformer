%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START MENU%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%VARIABLES FOR THE START MENU%%%%%%%%%%%%%%%%%%%%
var gameFirstStartup : boolean := false
var startNewGame : boolean := false
var resumeGame : boolean := false
var dropDown := false
var exitGame : boolean := false
var levelsSelect : boolean := false
var characterOptions : boolean := false
var levelOptions : boolean := false
var button1 : boolean := false
var button2 : boolean := false
var button3 : boolean := false
var buttonHold : boolean := false
var startButtonX, startButtonY, characterButtonX, characterButtonY, ballButtonX, ballButtonY, manButtonX, manButtonY, resumeButtonX, resumeButtonY : int
var levelsButtonX, levelsButtonY, exitButtonX, exitButtonY, button1X, button1Y, button2X, button2Y, button3X, button3Y, optionsButtonX, optionsButtonY, levelButtonX, levelButtonY : int
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%SETS LOCATIONS OF BUTTONS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
resumeButtonX := 70
resumeButtonY := 700
startButtonX := 70
startButtonY := 630
optionsButtonX := 70
optionsButtonY := 520
characterButtonX := optionsButtonX + 50
characterButtonY := optionsButtonY - 55
ballButtonX := 750
ballButtonY := 385
manButtonX := 950
manButtonY := ballButtonY
exitButtonX := 70
exitButtonY := 320
button1X := 705
button1Y := ballButtonY - 60
button2X := button1X + 175
button2Y := button1Y
button3X := button2X + 175
button3Y := button1Y
levelButtonX := optionsButtonX + 50
levelButtonY := optionsButtonY - 105
levelsButtonX := 70
levelsButtonY := 575
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%LOCATIONS OF LEVEL BUTTONS AND VARIABLES%%%%%%%%%%%%%%%%%%%%%%
var levelX : array 1 .. 5 of int
var levelY : array 1 .. 5 of int
var levelSelected : boolean := false
var selectedLevel : int
var newColor := RGB.AddColor (1.0, 0.0, 0.0)
for i : 0 .. 4
    levelX (i + 1) := 700 + (i * 100)
    levelY (i + 1) := 635
end for
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%THE LEVELS MENU PROCEDURE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc levelsMenu

    %%%%%%%%%%DRAWS THE ORIGINAL IMAGE OF LEVEL BUTTONS%%%%%%%%%%%%%%
    for i : 0 .. 4
	drawfillbox (levelX (i + 1), 620, levelX (i + 1) + 50, 670, 7)
	drawbox (levelX (i + 1), 620, levelX (i + 1) + 50, 670, 20)
    end for
    Font.Draw ("1", levelX (1) + 15, levelY (1), font2, 23)
    Font.Draw ("2", levelX (2) + 15, levelY (2), font2, 23)
    Font.Draw ("3", levelX (3) + 15, levelY (3), font2, 23)
    Font.Draw ("4", levelX (4) + 15, levelY (4), font2, 23)
    Font.Draw ("5", levelX (5) + 15, levelY (5), font2, 23)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%%%FOR LOOP OF 5 FOR 5 LEVELS%%%%%%%%%%%%%%%%%%%%%
    for i : 1 .. 5

	%%%%%%%%%%%%%%%%%%%%IF THE MOUSE IS OVER THE BUTTON AND HELD DOWN THEN SELECTS THAT LEVEL AND EXITS PROCEDURE%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if mouseX >= levelX (i) and mouseX <= levelX (i) + 50 and mouseY >= levelY (i) - 15 and mouseY <= levelY (i) + 50 and mouseButton not= 0 then
	    drawfillbox (levelX (i), 620, levelX (i) + 50, 670, 7)
	    drawbox (levelX (i), 620, levelX (i) + 50, 670, colour1)
	    if i = 1 then
		Font.Draw ("1", levelX (1) + 15, levelY (1), font2, colour1)
	    elsif i = 2 then
		Font.Draw ("2", levelX (2) + 15, levelY (2), font2, colour1)
	    elsif i = 3 then
		Font.Draw ("3", levelX (3) + 15, levelY (3), font2, colour1)
	    elsif i = 4 then
		Font.Draw ("4", levelX (4) + 15, levelY (4), font2, colour1)
	    elsif i = 5 then
		Font.Draw ("5", levelX (5) + 15, levelY (5), font2, colour1)
	    end if
	    View.Update
	    delay (500)
	    selectedLevel := i
	    levelSelected := true
	    exit
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%IF THE MOUSE IS HOVERED OVER A BUTTON THEN IT LIGHTS UP%%%%%%%
	if mouseX >= levelX (i) and mouseX <= levelX (i) + 50 and mouseY >= levelY (i) - 15 and mouseY <= levelY (i) + 50 then
	    drawfillbox (levelX (i), 620, levelX (i) + 50, 670, 7)
	    drawbox (levelX (i), 620, levelX (i) + 50, 670, colour1)
	    if i = 1 then
		Font.Draw ("1", levelX (1) + 15, levelY (1), font2, colour1)
	    elsif i = 2 then
		Font.Draw ("2", levelX (2) + 15, levelY (2), font2, colour1)
	    elsif i = 3 then
		Font.Draw ("3", levelX (3) + 15, levelY (3), font2, colour1)
	    elsif i = 4 then
		Font.Draw ("4", levelX (4) + 15, levelY (4), font2, colour1)
	    elsif i = 5 then
		Font.Draw ("5", levelX (5) + 15, levelY (5), font2, colour1)
	    end if
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%OTHERWISE IT JUST DRAWS GREY%%%%%%%%%%%%%%%%%%%%%%
	else
	    drawfillbox (levelX (i), 620, levelX (i) + 50, 670, 7)
	    drawbox (levelX (i), 620, levelX (i) + 50, 670, 20)
	    if i = 1 then
		Font.Draw ("1", levelX (1) + 15, levelY (1), font2, 23)
	    elsif i = 2 then
		Font.Draw ("2", levelX (2) + 15, levelY (2), font2, 23)
	    elsif i = 3 then
		Font.Draw ("3", levelX (3) + 15, levelY (3), font2, 23)
	    elsif i = 4 then
		Font.Draw ("4", levelX (4) + 15, levelY (4), font2, 23)
	    elsif i = 5 then
		Font.Draw ("5", levelX (5) + 15, levelY (5), font2, 23)
	    end if
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    end for
end levelsMenu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START MENU PROCEDURE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proc startMenu
    loop
	drawfillbox (0, 0, maxx, maxy, 7)

	%%%%%%%%%%%%%%%%%%%%%%%%IMPORTANT: GETS LOCATION OF MOUSE AND STORES IN VARIABLES%%%%%%%%%%%%%%%%%%%%
	Mouse.Where (mouseX, mouseY, mouseButton)
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%IF YOU'VE WON THE GAME THEN IT SAYS THIS%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if gameWon = true or gameWon2 = true then
	    Font.Draw ("THANKS PLAYER, BUT OUR PRINCESS IS IN ANOTHER CASTLE!", exitButtonX, exitButtonY - 300, font2, 43)
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%TO PREVENT FLICKERING OF A BUTTON, THIS BOOLEAN IS USED%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if mouseButton = 0 then
	    buttonHold := false
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%THIS STYLE OF CODE IS USED THROUGHOUT THIS PROCEDURE BUT NOT IN ORDER%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%THIS DRAWS THE BUTTON AND LIGHTS IT UP WHEN THE MOUSE IS OVER IT%%%%%%%%%%%%%%%%%%%%
	if mouseX >= exitButtonX and mouseX <= exitButtonX + 70 and mouseY >= exitButtonY and mouseY <= exitButtonY + 35 then
	    Font.Draw ("EXIT", exitButtonX, exitButtonY, font2, colour1)
	else
	    Font.Draw ("EXIT", exitButtonX, exitButtonY, font2, 23)
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%IF THE MOUSE IS HOVERED OVER AND CLICKED DOWN THEN IT SETS SOMETHING TRUE AND EXITS STARTMENU%%%%%%%%%%%%%%%%%%%%%%%%%
	if mouseX >= exitButtonX and mouseX <= exitButtonX + 130 and mouseY >= exitButtonY and mouseY <= exitButtonY + 35 and mouseButton not= 0 then
	    exitGame := true
	    exit
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%HERE THERE ARE TO SEPERATE THINGS THAT HAPPEN, IF YOU HAVE SOMETHING TO RESUME TO THEN%%%%%%%%%%%%%%%%%%%%%%%%%%
	if gameFirstStartup = true then
	    if mouseX >= resumeButtonX and mouseX <= resumeButtonX + 200 and mouseY >= resumeButtonY and mouseY <= resumeButtonY + 35 then
		Font.Draw ("RESUME", resumeButtonX, resumeButtonY, font1, colour1)
	    else
		Font.Draw ("RESUME", resumeButtonX, resumeButtonY, font1, 23)
	    end if

	    if mouseX >= startButtonX and mouseX <= startButtonX + 200 and mouseY >= startButtonY and mouseY <= startButtonY + 35 then
		Font.Draw ("NEW GAME", startButtonX, startButtonY, font2, colour1)
	    else
		Font.Draw ("NEW GAME", startButtonX, startButtonY, font2, 23)
	    end if

	    if mouseX >= optionsButtonX and mouseX <= optionsButtonX + 495 and mouseY >= optionsButtonY and mouseY <= optionsButtonY + 30 then
		Font.Draw ("CHARACTER & LEVEL OPTIONS", optionsButtonX, optionsButtonY, font2, colour1)
	    else
		Font.Draw ("CHARACTER & LEVEL OPTIONS", optionsButtonX, optionsButtonY, font2, 23)
	    end if
	    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%OTHERWISE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	else
	    if mouseX >= startButtonX and mouseX <= startButtonX + 260 and mouseY >= startButtonY and mouseY <= startButtonY + 35 then
		Font.Draw ("NEW GAME", startButtonX, startButtonY, font1, colour1)
	    else
		Font.Draw ("NEW GAME", startButtonX, startButtonY, font1, 23)
	    end if

	    if mouseX >= optionsButtonX and mouseX <= optionsButtonX + 495 and mouseY >= optionsButtonY and mouseY <= optionsButtonY + 30 then
		Font.Draw ("CHARACTER & LEVEL OPTIONS", optionsButtonX, optionsButtonY, font2, colour1)
	    else
		Font.Draw ("CHARACTER & LEVEL OPTIONS", optionsButtonX, optionsButtonY, font2, 23)
	    end if
	end if
	if mouseX >= levelsButtonX and mouseX <= characterButtonX + 240 and mouseY >= levelsButtonY and mouseY <= levelsButtonY + 30 then
	    Font.Draw ("LEVELS SELECT", levelsButtonX, levelsButtonY, font2, colour1)
	else
	    Font.Draw ("LEVELS SELECT", levelsButtonX, levelsButtonY, font2, 23)
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%LEVELS SELECT BUTTON%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if levelsSelect = false and buttonHold = false then
	    if mouseX >= levelsButtonX and mouseX <= levelsButtonX + 400 and mouseY >= levelsButtonY and mouseY <= levelsButtonY + 30 and mouseButton not= 0 then
		levelsSelect := true
		dropDown := false
		buttonHold := true
	    end if
	end if
	if levelsSelect = true then
	    levelsMenu
	end if
	if mouseX >= levelsButtonX and mouseX <= levelsButtonX + 400 and mouseY >= levelsButtonY and mouseY <= levelsButtonY + 30 and mouseButton not= 0 and buttonHold = false then
	    levelsSelect := false
	    buttonHold := true
	end if

	if levelSelected = true then
	    gameFirstStartup := true
	    levelsSelect := false
	    exit
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%THIS IS WHAT MAKES THE DROWDOWN MENU WORK%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if dropDown = false and buttonHold = false then
	    if mouseX >= optionsButtonX and mouseX <= optionsButtonX + 495 and mouseY >= optionsButtonY and mouseY <= optionsButtonY + 30 and mouseButton not= 0 then
		dropDown := true
		buttonHold := true
	    end if
	elsif dropDown = true and buttonHold = false then
	    if mouseX >= optionsButtonX and mouseX <= optionsButtonX + 495 and mouseY >= optionsButtonY and mouseY <= optionsButtonY + 30 and mouseButton not= 0 then
		dropDown := false
		buttonHold := true
	    end if
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if dropDown = true then
	    levelsSelect := false %CLOSES OTHER MENU
	    if mouseX >= characterButtonX and mouseX <= characterButtonX + 165 and mouseY >= characterButtonY and mouseY <= characterButtonY + 30 then
		Font.Draw ("CHARACTER", characterButtonX, characterButtonY, font3, colour1)
	    else
		Font.Draw ("CHARACTER", characterButtonX, characterButtonY, font3, 23)
	    end if
	    if mouseX >= levelButtonX and mouseX <= levelButtonX + 85 and mouseY >= levelButtonY and mouseY <= levelButtonY + 30 then
		Font.Draw ("LEVEL", levelButtonX, levelButtonY, font3, colour1)
	    else
		Font.Draw ("LEVEL", levelButtonX, levelButtonY, font3, 23)
	    end if

	    if characterOptions = false and buttonHold = false then
		if mouseX >= characterButtonX and mouseX <= characterButtonX + 165 and mouseY >= characterButtonY and mouseY <= characterButtonY + 30 and mouseButton not= 0 then
		    characterOptions := true
		    levelOptions := false
		    button1 := true
		    button2 := false
		    button3 := false
		    buttonHold := true
		end if
	    elsif characterOptions = true and buttonHold = false then
		if mouseX >= characterButtonX and mouseX <= characterButtonX + 165 and mouseY >= characterButtonY and mouseY <= characterButtonY + 30 and mouseButton not= 0 then
		    characterOptions := false
		    buttonHold := true
		end if
	    end if
	    if levelOptions = false and buttonHold = false then
		if mouseX >= levelButtonX and mouseX <= levelButtonX + 85 and mouseY >= levelButtonY and mouseY <= levelButtonY + 30 and mouseButton not= 0 then
		    levelOptions := true
		    characterOptions := false
		    button1 := true
		    button2 := false
		    button3 := false
		    buttonHold := true
		end if
	    elsif characterOptions = true and buttonHold = false then
		if mouseX >= levelButtonX and mouseX <= levelButtonX + 85 and mouseY >= levelButtonY and mouseY <= levelButtonY + 30 and mouseButton not= 0 then
		    levelOptions := false
		    buttonHold := true
		end if
	    end if

	    if characterOptions = true then
		if mouseX >= ballButtonX and mouseX <= ballButtonX + 120 and mouseY >= ballButtonY and mouseY <= ballButtonY + 25 or choice = "ball" then
		    Font.Draw ("BALL", ballButtonX, ballButtonY, font4, colour1)
		    playerX := ballButtonX + 105
		    playerY := ballButtonY
		    playerWidth := 20
		    DrawBall %
		    ColourChoice %FOR THE COLOR PALETTE
		    preview %
		else
		    Font.Draw ("BALL", ballButtonX, ballButtonY, font4, 23)
		    playerX := ballButtonX + 105
		    playerY := ballButtonY
		    playerWidth := 20
		    DrawBall
		    ColourChoice
		    preview
		end if

		if mouseX >= manButtonX and mouseX <= manButtonX + 120 and mouseY >= manButtonY and mouseY <= manButtonY + 25 or choice = "man" then
		    Font.Draw ("MAN", manButtonX, manButtonY, font4, colour1)
		    playerX := manButtonX + 105
		    playerY := manButtonY
		    playerWidth := 10
		    DrawMan
		    ColourChoice
		    preview
		else
		    Font.Draw ("MAN", manButtonX, manButtonY, font4, 23)
		    playerX := manButtonX + 105
		    playerY := manButtonY
		    playerWidth := 10
		    DrawMan
		    ColourChoice
		    preview
		end if

		if mouseX >= button1X and mouseX <= button1X + 95 and mouseY >= button1Y and mouseY <= button1Y + 15 or button1 then
		    Font.Draw ("COLOUR #1", button1X, button1Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("COLOUR #1", button1X, button1Y, font5, 23)
		    ColourChoice
		    preview
		end if

		if button1 = false then
		    if mouseX >= button1X and mouseX <= button1X + 95 and mouseY >= button1Y and mouseY <= button1Y + 15 and mouseButton not= 0 then
			button1 := true
			button2 := false
			button3 := false
		    end if
		end if

		if mouseX >= button2X and mouseX <= button2X + 95 and mouseY >= button2Y and mouseY <= button2Y + 15 or button2 then
		    Font.Draw ("COLOUR #2", button2X, button2Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("COLOUR #2", button2X, button2Y, font5, 23)
		    ColourChoice
		    preview
		end if

		if button2 = false then
		    if mouseX >= button2X and mouseX <= button2X + 95 and mouseY >= button2Y and mouseY <= button2Y + 15 and mouseButton not= 0 then
			button1 := false
			button2 := true
			button3 := false
		    end if
		end if

		if mouseX >= button3X and mouseX <= button3X + 75 and mouseY >= button3Y and mouseY <= button3Y + 15 or button3 then
		    Font.Draw ("OUTLINE", button3X, button3Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("OUTLINE", button3X, button3Y, font5, 23)
		    ColourChoice
		    preview
		end if

		if button3 = false then
		    if mouseX >= button3X and mouseX <= button3X + 75 and mouseY >= button3Y and mouseY <= button3Y + 15 and mouseButton not= 0 then
			button1 := false
			button2 := false
			button3 := true
		    end if
		end if
	    end if

	    if levelOptions = true then
		if mouseX >= button1X and mouseX <= button1X + 100 and mouseY >= button1Y and mouseY <= button1Y + 15 or button1 then
		    Font.Draw ("PLATFORMS", button1X, button1Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("PLATFORMS", button1X, button1Y, font5, 23)
		    ColourChoice
		    preview
		end if
		if button1 = false then
		    if mouseX >= button1X and mouseX <= button1X + 100 and mouseY >= button1Y and mouseY <= button1Y + 15 and mouseButton not= 0 then
			button1 := true
			button2 := false
			button3 := false
		    end if
		end if
		if mouseX >= button2X and mouseX <= button2X + 120 and mouseY >= button2Y and mouseY <= button2Y + 15 or button2 then
		    Font.Draw ("BACKGROUND", button2X, button2Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("BACKGROUND", button2X, button2Y, font5, 23)
		    ColourChoice
		    preview
		end if
		if button2 = false then
		    if mouseX >= button2X and mouseX <= button2X + 120 and mouseY >= button2Y and mouseY <= button2Y + 15 and mouseButton not= 0 then
			button1 := false
			button2 := true
			button3 := false
		    end if
		end if
		if mouseX >= button3X and mouseX <= button3X + 70 and mouseY >= button3Y and mouseY <= button3Y + 15 or button3 then
		    Font.Draw ("BORDER", button3X, button3Y, font5, colour1)
		    ColourChoice
		    preview
		else
		    Font.Draw ("BORDER", button3X, button3Y, font5, 23)
		    ColourChoice
		    preview
		end if
		if button3 = false then
		    if mouseX >= button3X and mouseX <= button3X + 70 and mouseY >= button3Y and mouseY <= button3Y + 15 and mouseButton not= 0 then
			button1 := false
			button2 := false
			button3 := true
		    end if
		end if
	    end if

	    if mouseX >= ballButtonX and mouseX <= ballButtonX + 125 and mouseY >= ballButtonY and mouseY <= ballButtonY + 35 and mouseButton not= 0 then
		if choice = "man" then
		    gameFirstStartup := false
		end if
		choice := "ball"
		playerX := maxx div 2
		exit
	    end if
	    if mouseX >= manButtonX and mouseX <= manButtonX + 125 and mouseY >= manButtonY and mouseY <= manButtonY + 35 and mouseButton not= 0 then
		if choice = "ball" then
		    gameFirstStartup := false
		end if
		choice := "man"
		playerX := maxx div 2
		exit
	    end if
	    if characterOptions = true then
		if button1 = true then
		    getColour1
		elsif button2 = true then
		    getColour2
		elsif button3 = true then
		    getColourOutline
		end if
	    elsif levelOptions = true then
		if button1 = true then
		    getColourPlatforms
		elsif button2 = true then
		    getColourBackground
		elsif button3 = true then
		    getColourBorder
		end if
	    end if
	end if

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%IF THE GAME HAS BEEN PREVIOUSLY RUN THEN%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if gameFirstStartup = true then
	    if mouseX >= resumeButtonX and mouseX <= resumeButtonX + 200 and mouseY >= resumeButtonY and mouseY <= resumeButtonY + 35 and mouseButton not= 0 then
		resumeGame := true
		playerX := maxx div 2
		playerY := playerYStoreValue
		exit
	    end if
	end if
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	if mouseX >= startButtonX and mouseX <= startButtonX + 255 and mouseY >= startButtonY and mouseY <= startButtonY + 35 and mouseButton not= 0 then
	    startNewGame := true
	    playerX := maxx div 2
	    gameFirstStartup := true
	    dropDown := false
	    characterOptions := false
	    levelOptions := false
	    exit
	end if
	
	View.Update
    end loop
end startMenu

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Again, for this procedure, there are two main things happening.  %
% 1. Drawing a button that lights up when the cursor is over it    %
% 2. Activating a button when it is clicked on                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
